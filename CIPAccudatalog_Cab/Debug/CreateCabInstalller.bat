@echo off
echo -- Copiar temporalmente las dlls --
echo CIPACCUDATALOG.resources.en.dll
copy "D:\sistemas\CIPACCUDATALOG_vs2008\CIPACCUDATALOG_PDA\obj\Debug\en\CIPACCUDATALOG.resources.dll" "D:\sistemas\CIPACCUDATALOG_vs2008\CIPACCUDATALOG_PDA\obj\Debug\en\CIPACCUDATALOG.resources.en.dll"
echo CIPACCUDATALOG.resources.es.dll
copy "D:\sistemas\CIPACCUDATALOG_vs2008\CIPACCUDATALOG_PDA\obj\Debug\es\CIPACCUDATALOG.resources.dll" "D:\sistemas\CIPACCUDATALOG_vs2008\CIPACCUDATALOG_PDA\obj\Debug\es\CIPACCUDATALOG.resources.es.dll"
echo CIPACCUDATALOG.resources.sw.dll
copy "D:\sistemas\CIPACCUDATALOG_vs2008\CIPACCUDATALOG_PDA\obj\Debug\sw\CIPACCUDATALOG.resources.dll" "D:\sistemas\CIPACCUDATALOG_vs2008\CIPACCUDATALOG_PDA\obj\Debug\sw\CIPACCUDATALOG.resources.sw.dll"
echo CIPACCUDATALOG.resources.uz.dll
copy "D:\sistemas\CIPACCUDATALOG_vs2008\CIPACCUDATALOG_PDA\obj\Debug\uz\CIPACCUDATALOG.resources.dll" "D:\sistemas\CIPACCUDATALOG_vs2008\CIPACCUDATALOG_PDA\obj\Debug\uz\CIPACCUDATALOG.resources.uz.dll"
echo -- Crear Cab Installer --
"C:\Program Files\Microsoft Visual Studio 9.0\smartdevices\sdk\sdktools\cabwiz.exe" "D:\sistemas\CIPACCUDATALOG_vs2008\CIPAccudatalog_CAB\Debug\CIPAccudatalog_CAB.inf" /dest "D:\sistemas\CIPACCUDATALOG_vs2008\CIPAccudatalog_CAB\Debug\" /err CabWiz.log
echo -- Eliminar copias temporales --
del "D:\sistemas\CIPACCUDATALOG_vs2008\CIPACCUDATALOG_PDA\obj\Debug\en\CIPACCUDATALOG.resources.en.dll"
del "D:\sistemas\CIPACCUDATALOG_vs2008\CIPACCUDATALOG_PDA\obj\Debug\es\CIPACCUDATALOG.resources.es.dll"
del "D:\sistemas\CIPACCUDATALOG_vs2008\CIPACCUDATALOG_PDA\obj\Debug\sw\CIPACCUDATALOG.resources.sw.dll"
del "D:\sistemas\CIPACCUDATALOG_vs2008\CIPACCUDATALOG_PDA\obj\Debug\uz\CIPACCUDATALOG.resources.uz.dll"
pause