'{######################################################################################################
'# Program Name : CIP Accudata Log
'# Module Name : Main module to register data
'# Version : 1.2
'# Purpose : To manage capture data, import/export from Excel
'# Dependencies: VSFLEX Grid of VideoSoft, Excel Data Reader Library v2.1, OpenNETCF Serial Library,
'# OpenFileDialogEx of MobilePractices, SharpZipLib, Symbol Library
'# Based on: ()
'# Reference: 
'# Data Started : 2013-10-01
'# Date Last modification : 2014-09-26
'# Copyright (c) 2013 by International Potato Center(CIP), Lima, Per�
'# License: GPL
'# Funding from: CIP
'# Contact: cip-riu@cgiar.org
'# Author: Edwin Rojas (analyst/software architect, team leader)
'# Author: Carlos velasquez (programmer)
'# Author: Reinhard Simon (Unit Head-RIU)
'########################################################################################################}

Imports OpenNETCF.IO.Serial.Port
Imports OpenNETCF.IO.Serial
Imports MobilePractices.OpenFileDialogEx
Imports Excel

Imports System.IO
Imports System.Data
Imports Symbol
Imports System.Reflection
Imports System.Resources

Imports System.Text
Imports Microsoft.VisualBasic.Strings

Imports System.Data.DataRow

Public Class Form_DataLog
    Private WithEvents SerialPort As OpenNETCF.IO.Serial.Port
    Dim curFile As String = "\My Documents\"

    Public Const bolUseScanner As Boolean = True
    'Public Const bolUseScanner As Boolean = False ' commented x jgr
    'Private MyAudioController As Symbol.Audio.Controller = Nothing

    Private MyReader As Symbol.Barcode.Reader = Nothing
    Private MyReaderData As Symbol.Barcode.ReaderData = Nothing
    Private MyReadNotifyHander As System.EventHandler = Nothing
    Private MyStatusNotifyHandler As System.EventHandler = Nothing

    Private WithEvents openFileDialog As OpenFileDialogEx = Nothing

    Private EvalError As Boolean = False
    Private EnterKeyEdit As Boolean = False
    Private EvalRow As Integer = 0 'remove
    Private EvalCol As Integer = 0 'remove

    Private myAssembly As System.Reflection.Assembly = Me.GetType.Assembly
    Private myManager As New System.Resources.ResourceManager("CIPACCUDATALOG.Resource", myAssembly)
    'System.Resources.ResourceManager(String.Format("{0}.{1}.{1}", myAssembly.GetName().Name, "Resources"), myAssembly)

    Private myset As MyAppSettings = New MyAppSettings

    Private currentValue As String


    '========Load Form=======
    Private Sub Form_DataLog_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles MyBase.Load

        openFileDialog = New OpenFileDialogEx

        'Disable Tabs
        Me.TabControl.TabPages(1).Enabled = True
        Me.TabControl.TabPages(2).Enabled = True

        'Settings by default
        Me.C1FlexGrid_DataCollector.AutoResize = True
        Me.C1FlexGrid_DataCollector.AllowDragging = False
        Me.C1FlexGrid_DataCollector.AllowSorting = False
        Me.C1FlexGrid_DataCollector.AllowFreezing = True

        If bolUseScanner = True Then
            'If we can initialize the Reader
            If (Me.InitReader()) Then
                'Start a read on the reader
                Me.StartRead() 'Desactivado para desarrollo
            Else
                'If not, close this form
                Me.Close()
                Return
            End If
        End If

        'Cargar las preferencias guardadas o las por defecto
        Dim value As String

        'Language
        value = myset.ListItems.Get(ComboBox_Language.Name)
        'MsgBox("Language new : " + value)

        Dim dt As New DataTable
        dt.Columns.Add("key")
        dt.Columns.Add("value")
        dt.Rows.Add("en", "en - English")
        dt.Rows.Add("es", "es - Spanish")
        dt.Rows.Add("sw", "sw - Swahili")
        dt.Rows.Add("uz", "zh - Chinese")
        ComboBox_Language.ValueMember = "key"
        ComboBox_Language.DisplayMember = "value"
        ComboBox_Language.DataSource = dt

        If String.IsNullOrEmpty(value) Then
            value = 0
            myset.ListItems.Set(ComboBox_Language.Name, "0")
        End If
        ComboBox_Language.SelectedIndex = CType(value, Integer)
        'FieldBook Template Type
        value = myset.ListItems.Get(ComboBox_FieldBook.Name)
        If String.IsNullOrEmpty(value) Then
            value = 0
            myset.ListItems.Set(ComboBox_FieldBook.Name, "0")
        End If
        ComboBox_FieldBook.SelectedIndex = CType(value, Integer)
        'COM Port Number
        value = myset.ListItems.Get(ComboBox_Port.Name)
        'MsgBox("port: " + value)
        If String.IsNullOrEmpty(value) Then
            value = 6
            myset.ListItems.Set(ComboBox_Port.Name, "6")
        End If
        ComboBox_Port.SelectedIndex = CType(value, Integer)
        'Label Design
        value = myset.ListItems.Get(ComboBox_LabelType.Name)
        'MsgBox("lbl design: " + value)
        If String.IsNullOrEmpty(value) Then
            value = 0
            myset.ListItems.Set(ComboBox_LabelType.Name, "0")
        End If
        ComboBox_LabelType.SelectedIndex = CType(value, Integer)
        'Input Form
        Try
            value = myset.ListItems.Get(CheckBox_InputForm.Name)
            CheckBox_InputForm.Checked = CType(value, Boolean)
        Catch ex As Exception
            CheckBox_InputForm.Checked = False
            myset.ListItems.Set(CheckBox_InputForm.Name, "0")
        End Try
        'Number of Fixed Rows of Input Form
        value = myset.ListItems.Get(NumericUpDownFrozenRowsNumberFom.Name)
        'MsgBox("num fixed rows: " + value)
        If String.IsNullOrEmpty(value) Then
            value = 1
            myset.ListItems.Set(NumericUpDownFrozenRowsNumberFom.Name, "1")
        End If
        NumericUpDownFrozenRowsNumberFom.Value = CType(value, Integer)
        'Number of Fixed Columns of DataCollector Grid
        value = myset.ListItems.Get(NumericUpDownFrozenColsNumber.Name)
        'MsgBox("num fixed cols: " + value)
        If String.IsNullOrEmpty(value) Then
            value = 1
            myset.ListItems.Set(NumericUpDownFrozenColsNumber.Name, "1")
        End If
        NumericUpDownFrozenColsNumber.Value = CType(value, Integer)
        'Replace Empty Cells
        Try
            value = myset.ListItems.Get(CheckBox_ReplaceBlankValue.Name)
            CheckBox_ReplaceBlankValue.Checked = CType(value, Boolean)
        Catch ex As Exception
            CheckBox_ReplaceBlankValue.Checked = False
            myset.ListItems.Set(CheckBox_ReplaceBlankValue.Name, "0")
        End Try
        'Empty Cell Value
        value = myset.ListItems.Get(TextBox_BlankValue.Name)
        TextBox_BlankValue.Text = value
        'Autosave
        Try
            value = myset.ListItems.Get(CheckBox_Autosave.Name)
            CheckBox_Autosave.Checked = CType(value, Boolean)
        Catch ex As Exception
            CheckBox_Autosave.Checked = True
            myset.ListItems.Set(CheckBox_Autosave.Name, "1")
        End Try

        'Notification
        Try
            value = myset.ListItems.Get(chkNotif.Name)
            chkNotif.Checked = CType(value, Boolean)
        Catch ex As Exception
            chkNotif.Checked = False
            myset.ListItems.Set(chkNotif.Name, "0")
        End Try

        'Missing value checkbox
        Try
            value = myset.ListItems.Get(chkMissing.Name)
            chkMissing.Checked = CType(value, Boolean)
        Catch ex As Exception
            chkMissing.Checked = False
            myset.ListItems.Set(chkMissing.Name, "0")
        End Try
        'Missing value textbox
        value = myset.ListItems.Get(txtMissing.Name)
        txtMissing.Text = value
        'myset.SaveXmlCurrentConfiguration()

        'If bolUseScanner = True Then
        '    ''para WAV
        '    'Select Device from device list
        '    Dim MyDevice As Symbol.Audio.Device = _
        '                                        Symbol.StandardForms.SelectDevice.Select( _
        '                                        Symbol.Audio.Controller.Title, _
        '                                        Symbol.Audio.Device.AvailableDevices)
        '    If (MyDevice Is Nothing) Then
        '        'Me.lblMessage.Text = "No Audio Device Selected"
        '        'Me.panMessage.Visible = True
        '    Else
        '        'check the device type
        '        Select Case (MyDevice.AudioType)
        '            Case Symbol.Audio.AudioType.StandardAudio
        '                MyAudioController = New Symbol.Audio.StandardAudio(MyDevice)

        '            Case Symbol.Audio.AudioType.SimulatedAudio
        '                MyAudioController = New Symbol.Audio.SimulatedAudio(MyDevice)
        '            Case Else
        '                Throw New Symbol.Exceptions.InvalidDataTypeException("Unknown Device Type")
        '        End Select
        '        'add event handler to handle event change
        '        'AddHandler MyAudioController.ChangeNotify, New EventHandler(AddressOf MyAudioController_ChangeNotify)
        '    End If
        'End If
    End Sub

#Region "ScanReader"

    'Initialize the reader.
    Private Function InitReader() As Boolean

        If bolUseScanner = False Then
            Return False
            Exit Function
        End If

        ' If reader is already present then fail initialize
        If Not (Me.MyReader Is Nothing) Then
            Return False
        End If

        Try
            'Get Selected device from user
            Dim MyDevice As Symbol.Generic.Device = Symbol.StandardForms.SelectDevice.Select( _
                                                        Symbol.Barcode.Device.Title, Symbol.Barcode.Device.AvailableDevices)
            If (MyDevice Is Nothing) Then
                'Me.lblMessage.Text = "No Device Selected, SelectDevice"
                'Me.panMessage.Visible = True
                Return False
            End If

            'create the reader, based on selected device
            Me.MyReader = New Symbol.Barcode.Reader(MyDevice)

            ' Create reader data
            Me.MyReaderData = New Symbol.Barcode.ReaderData(Symbol.Barcode.ReaderDataTypes.Text, Symbol.Barcode.ReaderDataLengths.MaximumLabel)

            'create event handler

            Me.MyReadNotifyHander = New EventHandler(AddressOf MyReader_ReadNotify)
            'Me.MyStatusNotifyHandler = New EventHandler(AddressOf MyReader_StatusNotify)

            'Enable reader, with wait cursor
            Me.MyReader.Actions.Enable()

        Catch ex As Symbol.Exceptions.InvalidRequestException
            ShowSystemMessage("InitReader" + vbCrLf + "Invalid Operation " + ex.Message)
            Return False

        Catch ex As Symbol.Exceptions.OperationFailureException
            ShowSystemMessage("InitReader" + vbCrLf + "Operation Failure " + ex.Message)
            Return False

        Catch ex As Symbol.Exceptions.UnimplementedFunctionException
            ShowSystemMessage("InitReader" + vbCrLf + "Unimplemented Function " + ex.Message)
            Return False

        Catch ex As Exception
            ShowSystemMessage("InitReader" + vbCrLf + "Exception " + ex.Message)
            Return False

        End Try

        '--------------------------------------------------------------------------------------------
        AddHandler Me.Activated, New EventHandler(AddressOf ReaderForm_Activated)
        AddHandler Me.Deactivate, New EventHandler(AddressOf ReaderForm_Deactivate)

        Return True
    End Function
    Private Sub MyReader_ReadNotify(ByVal sender As Object, ByVal e As EventArgs)
        'Get ReaderData
        Dim TheReaderData As Symbol.Barcode.ReaderData = Me.MyReader.GetNextReaderData

        Select Case TheReaderData.Result

            Case Symbol.Results.SUCCESS
                'Handle the data from this read
                Me.HandleData(TheReaderData)
                Me.StartRead()

            Case Symbol.Results.CANCELED

            Case Else
                'Dim sMsg As String
                'sMsg = "Read Failed\n" + "Result = " + (CInt(TheReaderData.Result)).ToString("X8")
                'Me.lblMessage.Text = sMsg
                'Me.panMessage.Visible = True
        End Select
    End Sub
    'Private Sub MyReader_StatusNotify(ByVal sender As Object, ByVal e As EventArgs)

    '    'Get Current status
    '    Dim TheEvent As Symbol.Barcode.BarcodeStatus = Me.MyReader.GetNextStatus

    '    'set event text in UI
    '    'Me.EventTextBox.Text = TheEvent.Text

    'End Sub

    ' Called when the form is activated.  This will occur when the form becomes the current application.
    Private Sub ReaderForm_Activated(ByVal sender As Object, ByVal e As EventArgs) 'Handles MyBase.Activated
        'If theStartReadre are no reads pending on MyReader start a new read
        Try
            If Not (Me.MyReaderData.IsPending) Then
                Me.StartRead()
            End If
        Catch ex As Exception

        End Try
    End Sub

    ' Called when the form is deactivated.  This will occur if the current application is switched to another
    ' running program.  This method halts attempting to read data from the reader.
    Private Sub ReaderForm_Deactivate(ByVal sender As Object, ByVal e As EventArgs) 'Handles MyBase.Deactivate
        Try
            Me.StopRead()
        Catch ex As Exception

        End Try
    End Sub

    Private Sub HandleData(ByVal TheReaderData As Symbol.Barcode.ReaderData)
        If bolUseScanner = False Then
            Exit Sub
        End If
        Call InterpretateBarcode(Trim(TheReaderData.Text))
    End Sub
    ' start a read on reader
    Private Sub StartRead()
        'if we have both a reader and readerdata
        If Not ((Me.MyReader Is Nothing) Or (Me.MyReaderData Is Nothing)) Then
            Try
                AddHandler MyReader.ReadNotify, Me.MyReadNotifyHander
                'Submit a read
                Me.MyReader.Actions.Read(Me.MyReaderData)
            Catch ex As Symbol.Exceptions.UnimplementedFunctionException
                'Me.lblMessage.Text = "StartRead\n" + "Unimplemented Function\n" + ex.Message()
                'Me.panMessage.Visible = True
            Catch ex As Symbol.Exceptions.InvalidIndexerException
                'Me.lblMessage.Text = "StartRead\n" + "Invalid Indexer\n" + ex.Message()
                'Me.panMessage.Visible = True
            Catch ex As Symbol.Exceptions.OperationFailureException
                'Me.lblMessage.Text = "StopRead\n" + "Operation Failure\n" + "Result = 0x" + (ex.Result).ToString("X8") + "\n" + ex.Message()
                'Me.panMessage.Visible = True
            Catch ex As Symbol.Exceptions.InvalidRequestException
                'Me.lblMessage.Text = "StartRead\n" + "Invalid Request\n" + ex.Message
                'Me.panMessage.Visible = True
            End Try
        End If
    End Sub
    ' stop all the read
    Private Sub StopRead()

        'if we do not have a reader, then do nothing
        If (Me.MyReader Is Nothing) Then
            Return
        End If
        Try
            'remove read notification handler
            RemoveHandler MyReader.ReadNotify, Me.MyReadNotifyHander

            'Flush (Cancel all pending reads)
            Me.MyReader.Actions.Flush()

        Catch ex As Symbol.Exceptions.UnimplementedFunctionException
            'Me.lblMessage.Text = "StartRead\n" + "Unimplemented Function\n" + ex.Message()
            'Me.panMessage.Visible = True
        Catch ex As Symbol.Exceptions.InvalidRequestException
            'Me.lblMessage.Text = "StartRead\n" + "Invalid Request\n" + ex.Message()
            'Me.panMessage.Visible = True
        Catch ex As Symbol.Exceptions.OperationFailureException
            'Me.lblMessage.Text = "StopRead\n" + "Operation Failure\n" + "Result = 0x" + (ex.Result).ToString("X8") + "\n" + ex.Message()
            'Me.panMessage.Visible = True
        End Try

    End Sub

#End Region

    Private Sub frmDataLog_Closing(ByVal sender As Object, ByVal e As System.ComponentModel.CancelEventArgs) Handles MyBase.Closing
        MyReader = Nothing
        MyReaderData = Nothing
        MyReadNotifyHander = Nothing
        MyStatusNotifyHandler = Nothing
    End Sub

    Sub InterpretateBarcode(ByVal strLabcode As String)

        Dim bolRead As Boolean
        bolRead = False
        Try
            If Me.TabControl.SelectedIndex = 1 Then
                Me.TextBox_barcode.Text = strLabcode
                Call ButtonBuscar_Click(Me.Button_Search, Nothing)
                bolRead = True
            End If
        Catch
        End Try
    End Sub

    '========================

    'Choose Excel File
    Private Sub Button_LoadTemplate_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button_SearchTemplate.Click

        Me.openFileDialog.Filter = "*.xls|*.xlsx"

        'load last location
        openFileDialog.InitialDirectory = myset.ListItems.Get(openFileDialog.Name)
        If openFileDialog.InitialDirectory = "" Or Directory.Exists(openFileDialog.InitialDirectory) = False Then
            openFileDialog.InitialDirectory = "\My Documents"
        End If

        Me.OpenFileDialog1.FileName = Nothing
        'Me.OpenFileDialog1.Filter = "Excel files (*.xls,*xlsx)|*.xls;*.xlsx"
        'Me.OpenFileDialog1.FilterIndex = 1
        Me.ListBoxColumns.Items.Clear()
        Me.ListBoxToCompare.Items.Clear()

        If C1FlexGrid_DataCollector.Cols.Count > 0 Then
            Me.C1FlexGrid_DataCollector.Clear()
            Me.C1FlexGrid_DataCollector.Cols.RemoveRange(0, C1FlexGrid_DataCollector.Cols.Count)
        End If

        If Me.openFileDialog.ShowDialog = DialogResult.OK Then
            Me.TextBoxFileName.Text = Path.GetFileName(Me.openFileDialog.FileName)
            Me.OpenFileDialog1.FileName = Me.openFileDialog.FileName
            'save location
            myset.ListItems.Set(openFileDialog.Name, Path.GetDirectoryName(openFileDialog.FileName))
            myset.SaveXmlCurrentConfiguration()
        Else
            Me.TextBoxFileName.Text = ""
            Exit Sub
        End If

        Dim stream As FileStream = File.Open(Me.OpenFileDialog1.FileName, FileMode.Open, FileAccess.Read)
        Dim excelReader As IExcelDataReader

        If Path.GetExtension(Me.TextBoxFileName.Text).ToLower().Equals(".xlsx") Then
            excelReader = ExcelReaderFactory.CreateOpenXmlReader(stream)
        ElseIf Path.GetExtension(Me.TextBoxFileName.Text).ToLower().Equals(".xls") Then
            excelReader = ExcelReaderFactory.CreateBinaryReader(stream)
        Else
            Me.TextBoxFileName.Text = ""
            MessageBox.Show("Please, upload a Excel File")
            Exit Sub
        End If

        Dim result As DataSet
        Dim objTable As DataTable
        Dim intRow As Integer, intCol As Integer

        'initial row number
        Dim RowTable As Integer
        Dim RowData As Integer

        curFile = Path.GetDirectoryName(Me.openFileDialog.FileName) + "\"

        Try
            'Custom Fielbook
            If Me.ComboBox_FieldBook.SelectedIndex = 1 Then

                Dim fName As String = Path.GetFileNameWithoutExtension(Me.openFileDialog.FileName)
                'Dim fExt = Path.GetExtension(Me.openFileDialog.FileName)
                curFile += fName + Date.Now.ToString(".yyyyMMdd_HHmmss") + ".xls"

                'hide panelInfoExtra
                If Me.PanelInfoExtra.Visible = True Then
                    Me.C1FlexGrid_DataCollector.Top = PanelInfoExtra.Top
                    Me.C1FlexGrid_DataCollector.Height += PanelInfoExtra.Height
                    Me.PanelInfoExtra.Visible = False
                End If

                result = excelReader.AsDataSet()

                RowTable = 0
                RowData = 1

                'Sheet1
                objTable = result.Tables(0)

                Dim countRows As Integer = 0
                Dim countCols As Integer = 0
                'count Cols
                For intCol = 0 To objTable.Columns.Count - 1
                    If objTable.Rows(RowTable).ItemArray(intCol) Is DBNull.Value Then
                        Exit For
                    End If
                    countCols += 1
                Next
                'count Rows
                For intRow = RowTable To objTable.Rows.Count - 1
                    If objTable.Rows(intRow).ItemArray(0) Is DBNull.Value Then
                        Exit For
                    End If
                    countRows += 1
                Next
                'redim grid
                C1FlexGrid_DataCollector.Cols.Count = countCols
                C1FlexGrid_DataCollector.Rows.Count = countRows
                C1FlexGrid_DataCollector.Clear()

                'Hide values format row
                Me.C1FlexGrid_DataCollector.Rows(1).Visible = False
                C1FlexGrid_DataCollector.Rows(1).AllowEditing = False

                C1FlexGrid_DataCollector.Cols.Frozen = NumericUpDownFrozenColsNumber.Value
                If NumericUpDownFrozenColsNumber.Value < C1FlexGrid_DataCollector.Cols.Count Then
                    For col As Integer = 0 To NumericUpDownFrozenColsNumber.Value - 1
                        C1FlexGrid_DataCollector.Cols(col).AllowEditing = False
                    Next
                    For col As Integer = NumericUpDownFrozenColsNumber.Value To C1FlexGrid_DataCollector.Cols.Count - 1
                        C1FlexGrid_DataCollector.Cols(col).AllowEditing = True
                    Next
                End If

                'Fill gridview
                For intRow = RowTable To RowTable + countRows - 1
                    For intCol = 0 To countCols - 1
                        C1FlexGrid_DataCollector(intRow - RowTable, intCol) = objTable.Rows(intRow).ItemArray(intCol)
                    Next
                Next

                Dim FormatCell As String()
                For intCol = 0 To objTable.Columns.Count - 1
                    Dim deb = C1FlexGrid_DataCollector(1, intCol).ToString().ToUpper()
                    If C1FlexGrid_DataCollector(1, intCol).ToString().ToUpper().StartsWith("CATEG") Then
                        FormatCell = C1FlexGrid_DataCollector(1, intCol).ToString().Split(":")
                        C1FlexGrid_DataCollector.Cols(intCol).ComboList = " |" + FormatCell(1)
                    End If
                Next

                'Load Columns Names to ListBoxColumns
                Dim i As Integer
                For i = 0 To C1FlexGrid_DataCollector.Cols.Count - 1
                    ListBoxColumns.Items.Add(i.ToString() + "|" + C1FlexGrid_DataCollector(0, i).ToString())
                Next

                'Search saved fieldbook

                If CheckBox_SearchForSavedFiles.Checked = True Then
                    ListView_SavedFiles.Items.Clear()
                    For Each File As String In Directory.GetFiles(Path.GetDirectoryName(Me.openFileDialog.FileName))
                        Dim FileName As String = Path.GetFileNameWithoutExtension(File)
                        Dim DotPosition As Integer = FileName.LastIndexOf(".")
                        If DotPosition > -1 Then
                            Dim FileNamePart1 As String = FileName.Substring(0, DotPosition) 'fieldbook name
                            'Dim FileNamePart2 As String = FileName.Substring(DotPosition, FileName.Length - DotPosition) 'timestamp
                            If FileNamePart1 = fName Then
                                'Creating the list items
                                Dim ListItem As ListViewItem = New ListViewItem(Path.GetFileName(File))
                                ListView_SavedFiles.Items.Add(ListItem)
                            End If
                        End If
                    Next


                    If ListView_SavedFiles.Items.Count > 0 Then
                        Panel_SavedFiles.Top = 0
                        Panel_SavedFiles.Left = 0
                        Panel_SavedFiles.Visible = True
                    End If

                End If

                'FieldBook Clone Selector
            ElseIf Me.ComboBox_FieldBook.SelectedIndex = 0 Then

                'show panelInfoExtra
                If Me.PanelInfoExtra.Visible = False Then
                    Me.C1FlexGrid_DataCollector.Top = PanelInfoExtra.Bottom
                    Me.C1FlexGrid_DataCollector.Height -= PanelInfoExtra.Height
                    Me.PanelInfoExtra.Visible = True
                End If

                RowTable = 22
                RowData = 24

                Dim ColTable As Integer = 1
                Dim GridCol As Integer = 0
                Dim GridRow As Integer = 2
                Dim ExcelRow As Integer = 0
                Dim countRows As Integer = 0
                Dim LowerLimits, UpperLimits As Integer()
                ReDim LowerLimits(106)
                ReDim UpperLimits(106)

                While excelReader.Read()
                    ' Get number of rows
                    If ExcelRow >= RowData And excelReader.GetString(1) <> "" Then
                        countRows += 1
                    End If
                    ' Get LowerLimits
                    If ExcelRow = 19 Then
                        GridCol = 6
                        Dim ExcelCol As Integer
                        For ExcelCol = 1 To 106
                            If (ExcelCol >= 26 And ExcelCol <= 106) Then
                                If IsNumeric(excelReader.GetString(ExcelCol)) Then
                                    LowerLimits(GridCol) = Integer.Parse(excelReader.GetString(ExcelCol))
                                Else
                                    LowerLimits(GridCol) = -1
                                End If
                                GridCol += 1
                            End If
                        Next
                    End If
                    ' Get UpperLimits
                    If ExcelRow = 20 Then
                        GridCol = 6
                        Dim ExcelCol As Integer
                        For ExcelCol = 1 To 106
                            If (ExcelCol >= 26 And ExcelCol <= 106) Then
                                If IsNumeric(excelReader.GetString(ExcelCol)) Then
                                    UpperLimits(GridCol) = Integer.Parse(excelReader.GetString(ExcelCol))
                                Else
                                    UpperLimits(GridCol) = -1
                                End If
                                GridCol += 1
                            End If
                        Next
                    End If
                    ExcelRow += 1
                End While

                'going to General Sheet
                excelReader.NextResult()
                excelReader.NextResult()
                excelReader.NextResult()
                'Setting curFile
                ExcelRow = 0
                While excelReader.Read()
                    If ExcelRow = 1 Then
                        If Not String.IsNullOrEmpty(excelReader.GetString(1)) Then
                            curFile += excelReader.GetString(1) + " " 'Short name or Title
                        Else
                            curFile += "shortname "
                        End If
                    ElseIf ExcelRow = 11 Then
                        If Not String.IsNullOrEmpty(excelReader.GetString(1)) Then
                            curFile += excelReader.GetString(1) 'Site Name
                        Else
                            curFile += "sitename"
                        End If
                    End If
                    ExcelRow += 1
                End While
                curFile += "." + Date.Now.ToString("yyyyMMdd_HHmmss") + ".xls"

                'Load excel reader again for loading data
                If Path.GetExtension(Me.TextBoxFileName.Text).ToLower().Equals(".xlsx") Then
                    excelReader = ExcelReaderFactory.CreateOpenXmlReader(stream)
                ElseIf Path.GetExtension(Me.TextBoxFileName.Text).ToLower().Equals(".xls") Then
                    excelReader = ExcelReaderFactory.CreateBinaryReader(stream)
                End If
                'Redim gridview
                C1FlexGrid_DataCollector.Cols.Count = 87
                C1FlexGrid_DataCollector.Rows.Count = countRows + 2

                ExcelRow = 0
                GridCol = 0
                ' Fill gridview
                ' Headers
                While (excelReader.Read())
                    If ExcelRow = RowTable Then
                        Dim ExcelCol As Integer
                        For ExcelCol = 1 To 106
                            If ((ExcelCol >= 1 And ExcelCol <= 4) Or (ExcelCol >= 6 And ExcelCol <= 7) Or (ExcelCol >= 26 And ExcelCol <= 106)) Then
                                C1FlexGrid_DataCollector(0, GridCol) = excelReader.GetString(ExcelCol)
                                If ExcelCol <= 7 Then
                                    C1FlexGrid_DataCollector.Cols(GridCol).AllowEditing = False

                                Else
                                    If LowerLimits(GridCol) > -1 And UpperLimits(GridCol) > -1 Then
                                        C1FlexGrid_DataCollector(1, GridCol) = "Range:" + LowerLimits(GridCol).ToString() + "-" + UpperLimits(GridCol).ToString()
                                    ElseIf LowerLimits(GridCol) > -1 And UpperLimits(GridCol) < 0 Then
                                        C1FlexGrid_DataCollector(1, GridCol) = "MoreThanEqual:" + LowerLimits(GridCol).ToString()
                                    End If

                                End If
                                GridCol += 1
                            End If
                        Next
                    End If
                    If ExcelRow = RowTable + 1 Then
                        GridCol = 0
                        Dim ExcelCol As Integer
                        For ExcelCol = 1 To 106
                            If ((ExcelCol >= 1 And ExcelCol <= 4) Or (ExcelCol >= 6 And ExcelCol <= 7) Or (ExcelCol >= 26 And ExcelCol <= 106)) Then
                                If excelReader.GetString(ExcelCol) <> "" Then
                                    C1FlexGrid_DataCollector(0, GridCol) += " " + excelReader.GetString(ExcelCol)
                                End If
                                GridCol += 1
                            End If
                        Next
                        ExcelRow += 1
                        Exit While
                    End If
                    ExcelRow += 1
                End While
                ' Body
                Me.C1FlexGrid_DataCollector.Rows(1).Visible = False
                While (excelReader.Read())

                    If ExcelRow >= RowData Then

                        If excelReader.GetString(1) = "" Then
                            Exit While
                        End If

                        GridCol = 0
                        Dim ExcelCol As Integer
                        For ExcelCol = 1 To 106
                            If ((ExcelCol >= 1 And ExcelCol <= 4) Or (ExcelCol >= 6 And ExcelCol <= 7) Or (ExcelCol >= 26 And ExcelCol <= 106)) Then
                                Dim cellvalue = excelReader.GetString(ExcelCol)
                                C1FlexGrid_DataCollector(GridRow, GridCol) = excelReader.GetString(ExcelCol)
                                GridCol += 1
                            End If
                        Next
                        GridRow += 1
                    End If
                    ExcelRow += 1
                End While

                'Move Plot Column to position 0
                C1FlexGrid_DataCollector.Cols().Move(2, 0)
                'freeze Plot Column
                C1FlexGrid_DataCollector.Cols().Frozen = 1

                'Set default Label Type
                Me.ComboBox_LabelType.SelectedIndex = 0

                'Load Columns Names to ListBoxColumns
                Dim i As Integer
                For i = 0 To C1FlexGrid_DataCollector.Cols.Count - 1
                    ListBoxColumns.Items.Add(i.ToString() + "|" + C1FlexGrid_DataCollector(0, i).ToString())
                Next
                'Set Columns to compare
                ListBoxToCompare.Items.Add(ListBoxColumns.Items(0).ToString())
                ListBoxColumns.Items.RemoveAt(0)

                'Set Colors to headers
                Dim cellRange As C1.Win.C1FlexGrid.CellRange = C1FlexGrid_DataCollector.GetCellRange(0, 6, 0, 15)
                Dim cs As C1.Win.C1FlexGrid.CellStyle = cellRange.StyleNew
                cs.BackColor = Color.FromArgb(146, 208, 80) 'lime
                cellRange.Style = cs
                cellRange = C1FlexGrid_DataCollector.GetCellRange(0, 16, 0, 31)
                cs = cellRange.StyleNew
                cs.BackColor = Color.FromArgb(192, 0, 0)    'red1
                cellRange.Style = cs
                cellRange = C1FlexGrid_DataCollector.GetCellRange(0, 32, 0, 42)
                cs = cellRange.StyleNew
                cs.BackColor = Color.FromArgb(255, 192, 0)  'yellow
                cellRange.Style = cs
                cellRange = C1FlexGrid_DataCollector.GetCellRange(0, 43, 0, 54)
                cs = cellRange.StyleNew
                cs.BackColor = Color.FromArgb(0, 176, 80)   'green
                cellRange.Style = cs
                cellRange = C1FlexGrid_DataCollector.GetCellRange(0, 55, 0, 68)
                cs = cellRange.StyleNew
                cs.BackColor = Color.FromArgb(0, 176, 240)  'skyblue
                cellRange.Style = cs
                cellRange = C1FlexGrid_DataCollector.GetCellRange(0, 69, 0, 75)
                cs = cellRange.StyleNew
                cs.BackColor = Color.FromArgb(255, 0, 0)    'red2
                cellRange.Style = cs
            End If

            C1FlexGrid_DataCollector.AutoSizeCols()
            excelReader.Close()

        Catch ex As Exception
            excelReader.Close()
            MsgBox("An error has occurred trying to load fieldbook template." + vbCrLf + "Please check if fieldbook template is correct.")
            Exit Sub
        End Try
    End Sub

    'PopulateArray
    Private Sub PopulateArray(ByVal strValue As String, ByRef arrayValues() As String)
        ReDim arrayValues(2)

        'Dim stream As FileStream = File.Open(Me.OpenFileDialog1.FileName, FileMode.Open, FileAccess.Read)
        'Dim excelReader As IExcelDataReader

        'If Path.GetExtension(Me.TextBox_CrossingPlan.Text).ToLower().Equals(".xlsx") Then
        'excelReader = ExcelReaderFactory.CreateOpenXmlReader(Stream)
        'Else
        'excelReader = ExcelReaderFactory.CreateBinaryReader(Stream)
        'End If

        'Dim result As DataSet = excelReader.AsDataSet()
        'Dim objTable As DataTable
        Dim intRow As Integer, intCol As Integer

        'objTable = result.Tables(0)

        'For intRow = 2 To objTable.Rows.Count - 1
        'If objTable.Rows(intRow).ItemArray(0) Is DBNull.Value And objTable.Rows(intRow).ItemArray(1) Then
        ' Exit For
        ' End If
        '
        'If objTable.Rows(intRow).ItemArray(0).ToString() = strValue Then
        ' arrayValues(0) = objTable.Rows(intRow).ItemArray(0).ToString
        'arrayValues(1) = objTable.Rows(intRow).ItemArray(1).ToString
        'Exit For
        'End If
        'Next

        'excelReader.Close()
        For intRow = 2 To C1FlexGrid_DataCollector.Rows.Count - 1
            If C1FlexGrid_DataCollector(intRow, 0).ToString() = strValue Then
                arrayValues(0) = C1FlexGrid_DataCollector(intRow, 0).ToString()
                arrayValues(1) = C1FlexGrid_DataCollector(intRow, 1).ToString()
                Exit For
            End If
        Next

    End Sub

    '======Print Options=====
    'Printing template
    Private Sub PrintLabel(ByVal strValues() As String, ByVal strBarcodeType As String, ByVal strPort As String, ByVal strPrintType As String, ByRef strError As String)
        Dim strStartOfLabel, strLabelText, strEndPrint As String
        'Dim GUID As String = getGUID()
        Dim qty As Integer
        qty = NumericUpDownCantidad.Value
        If strPrintType = "Test" Then
            strStartOfLabel = "! 0 200 200 250 1" + vbNewLine
            'strLabelText = "T 0 5 1202 3 " + strValues(0) + vbNewLine
            strLabelText = "T 7 0 40 40 " + strValues(0) + vbNewLine
            strLabelText = strLabelText + "T 7 0 40 70 " + strValues(1) + vbNewLine
            'strLabelText = strLabelText + "T 7 0 40 10 " + strValues(1) + vbNewLine
            'strLabelText = strLabelText + "T 7 0 40 40 " + strValues(2) + vbNewLine
            'strLabelText = strLabelText + "T 7 0 40 70 " + strValues(3) + vbNewLine
            'strLabelText = strLabelText + "T 7 0 40 100 " + strValues(4) + "(C)" + vbNewLine
            'If Trim(LCase(strBarcodeType)) = "1d" Then
            'strLabelText = strLabelText + "B 128 1 1 40 40 140 " + strValues(0) + vbNewLine
            'Else
            strLabelText = strLabelText + "B DATAMATRIX 15520 20 H 6 S 200 " + vbNewLine
            strLabelText = strLabelText + strValues(1) + vbNewLine
            strLabelText = strLabelText + "ENDDATAMATRIX " + vbNewLine

            strEndPrint = "PRINT" + vbNewLine
            Call PrintRowLabels(strStartOfLabel + strLabelText + strEndPrint, strPort, strError)
        End If
        If strPrintType = "Test2" Then
            strStartOfLabel = "! 0 200 200 250 1" + vbNewLine
            'strLabelText = "T 0 5 1202 3 " + strValues(0) + vbNewLine
            strLabelText = "T 7 0 40 20 " + "Plot No: " + strValues(0) + vbNewLine
            strLabelText = strLabelText + "T 7 0 40 60 " + "Genotype: " + strValues(1) + vbNewLine
            strLabelText = strLabelText + "T 7 0 40 100 " + "Vines/Plot: " + strValues(2) + vbNewLine
            strLabelText = strLabelText + "T 7 0 40 140 " + "Site name: " + strValues(3) + vbNewLine
            strLabelText = strLabelText + "B DATAMATRIX 15520 20 H 6 S 200 " + vbNewLine
            strLabelText = strLabelText + strValues(1) + vbNewLine
            strLabelText = strLabelText + "ENDDATAMATRIX " + vbNewLine

            strEndPrint = "PRINT" + vbNewLine
            Call PrintRowLabels(strStartOfLabel + strLabelText + strEndPrint, strPort, strError)
        End If
        If strPrintType = "CloneSelector1" Then
            strStartOfLabel = "! 0 200 200 250 " + qty.ToString() + vbNewLine
            strLabelText = "LABEL" + vbNewLine
            strLabelText = strLabelText + "PAGE-WIDTH 420" + vbNewLine
            strLabelText = strLabelText + "PAGE-HEIGHT 250" + vbNewLine
            strLabelText = strLabelText + "T 5 0 40 10 " + "Trial: " + strValues(0) + vbNewLine
            strLabelText = strLabelText + "T 5 0 40 35 " + "Plot No: " + strValues(1) + vbNewLine
            strLabelText = strLabelText + "T 5 0 40 60 " + "Genotype: " + strValues(2) + vbNewLine
            strLabelText = strLabelText + "T 5 0 40 85 " + "Rep. No: " + strValues(3) + vbNewLine
            strLabelText = strLabelText + "T 5 0 40 110 " + "Activity: " + strValues(4) + vbNewLine
            strLabelText = strLabelText + "T 5 0 40 135 " + "Date: " + strValues(5) + vbNewLine
            strLabelText = strLabelText + "B DATAMATRIX 290 20 H 8 S 100" + vbNewLine
            strLabelText = strLabelText + strValues(1) + vbNewLine
            strLabelText = strLabelText + "ENDDATAMATRIX " + vbNewLine
            strLabelText = strLabelText + "B 128 1 1 20 290 145 " + strValues(1) + vbNewLine
            'strLabelText = strLabelText + "T 0 0 300 170 " + strValues(1) + vbNewLine
            strLabelText = strLabelText + "T 0 0 290 170 " + "Site: " + strValues(6) + vbNewLine
            strEndPrint = "PRINT" + vbNewLine
            Call PrintRowLabels(strStartOfLabel + strLabelText + strEndPrint, strPort, strError)
        End If
    End Sub
    'Printing characteristics
    Public Sub PrintRowLabels(ByVal strCommands As String, ByVal strPort As String, ByRef strError As String)
        Dim intIndex As Int16

        strError = ""
        SerialPort = New OpenNETCF.IO.Serial.Port(strPort)

        SerialPort.Settings.BaudRate = 19200
        SerialPort.Settings.ByteSize = 8
        SerialPort.Settings.StopBits = OpenNETCF.IO.Serial.StopBits.one
        SerialPort.Settings.Parity = OpenNETCF.IO.Serial.Parity.none

        Try
            'If SerialPort.IsOpen Then
            SerialPort.Open()
            'Else
            'strError = "1"
            'Call ShowSystemMessage("Please, enable bluetooth or turn on the printer...")
            'Exit Sub
            'End If

        Catch ex As Exception
            'MessageBox.Show("Please, turn on printer...")
            Call ShowSystemMessage("Please, turn on printer...")
            Exit Sub
        End Try

        Dim arrayOutput As Byte() = New Byte(0) {}

        For intIndex = 0 To strCommands.Length - 1
            arrayOutput(0) = Convert.ToByte(strCommands(intIndex))
            SerialPort.Output = arrayOutput
        Next

        SerialPort.Close()
        If SerialPort IsNot Nothing Then
            SerialPort.Dispose()
        End If
    End Sub

    'Button Close
    Private Sub Button_Close_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button_Close.Click
        Me.Panel_Message.Visible = False
    End Sub

    '========================
    '===Show System Message==

    Sub ShowSystemMessage(ByVal strMessage As String)
        Panel_Message.Top = 0
        Panel_Message.Left = 0
        Panel_Message.Height = Me.Height
        Panel_Message.Width = Me.Width
        Label_Message.Text = strMessage
        Panel_Message.Visible = True
    End Sub
    Private Sub C1FlexGrid_DataColletor(ByVal sender As System.Object, ByVal e As C1.Win.C1FlexGrid.RowColEventArgs) Handles C1FlexGrid_DataCollector.AfterEdit
        EvalError = False
        EvalRow = e.Row
        EvalCol = e.Col

        Dim ok As Boolean = True
        Dim Message As String = ""

        Dim CellValue As Object = C1FlexGrid_DataCollector(e.Row, e.Col)
        Dim CellFormat As Object = C1FlexGrid_DataCollector(1, e.Col)
        Dim boolPass As Boolean
        boolPass = False

        If chkMissing.Checked And CellValue.ToString() = txtMissing.Text Then
            boolPass = True
        Else
            Validate_Cells(CellValue, CellFormat, Message)

            If EvalError = True Then
                'Desactivar o activar otros controles
                Button_Save.Enabled = False
                Panel_Message.Visible = True
                Panel_Message.Left = 0
                Panel_Message.Top = 0
                Panel_Message.Height = Me.C1FlexGrid_DataCollector.Top
                Panel_Message.Width = Me.Width
                Label_Message.Text = Message

                e.Cancel = True

                C1FlexGrid_DataCollector.Focus()
                C1FlexGrid_DataCollector.Select(e.Row, e.Col)
                'MessageBox.Show(Message)
                C1FlexGrid_DataCollector.StartEditing(e.Row, e.Col)
            Else
                boolPass = True
                

            End If
        End If
        If boolPass Then
            If chkNotif.Checked Then
                If currentValue <> C1FlexGrid_DataCollector(e.Row, e.Col).ToString() Then
                    Dim LResponse As Integer
                    LResponse = MsgBox("Are you sure that you want to change the cell value?", vbYesNo)
                    If LResponse = vbNo Then
                        C1FlexGrid_DataCollector(e.Row, e.Col) = currentValue
                    End If
                End If
            End If

            Button_Save.Enabled = True
            Panel_Message.Visible = False

            C1FlexGrid_DataCollector.Focus()

            If EnterKeyEdit = True Then
                If e.Col < C1FlexGrid_DataCollector.Cols.Count - 1 Then
                    C1FlexGrid_DataCollector.Select(e.Row, e.Col + 1)
                ElseIf e.Row < C1FlexGrid_DataCollector.Rows.Count - 1 Then
                    C1FlexGrid_DataCollector.Select(e.Row + 1, C1FlexGrid_DataCollector.Cols.Frozen)
                End If
                EnterKeyEdit = False
            End If

            'Save Changes
            If (CheckBox_Autosave.Checked = True) Then
                ButtonSaveFieldBook_Click(Nothing, Nothing)
            End If
        End If
    End Sub

    Private Sub ButtonSaveFieldBook_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button_Save.Click
        'If fieldbook type is CloneSelector change columns before saving
        If (Me.ComboBox_FieldBook.SelectedIndex = 0) Then
            C1FlexGrid_DataCollector.Cols().Move(0, 2)
        End If

        Dim detailsNameWrite As String = curFile
        Dim objWriter As New System.IO.StreamWriter(detailsNameWrite)

        Dim iCol, iRow As Integer
        Dim line As String

        'write headers
        line = ""
        For iCol = 0 To C1FlexGrid_DataCollector.Cols.Count - 1
            line += C1FlexGrid_DataCollector(0, iCol) + Chr(9)
        Next
        objWriter.WriteLine(line)
        'write validation formats
        line = ""
        For iCol = 0 To C1FlexGrid_DataCollector.Cols.Count - 1
            line += C1FlexGrid_DataCollector(1, iCol) + Chr(9)
        Next
        objWriter.WriteLine(line)
        'write data
        For iRow = 2 To C1FlexGrid_DataCollector.Rows.Count - 1
            line = ""
            For iCol = 0 To C1FlexGrid_DataCollector.Cols.Count - 1
                If IsDBNull(C1FlexGrid_DataCollector(iRow, iCol)) Then
                    line += Chr(9)
                Else
                    line += C1FlexGrid_DataCollector(iRow, iCol) + Chr(9)
                End If
            Next
            objWriter.WriteLine(line)
        Next

        objWriter.Close()
        'If fieldbook type is CloneSelector change columns after saving
        If (Me.ComboBox_FieldBook.SelectedIndex = 0) Then
            C1FlexGrid_DataCollector.Cols().Move(2, 0)
        End If

        'MessageBox.Show("Filebook has been saved")
    End Sub

    Private Sub ButtonPrint_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button_Print.Click

        Dim arrayValues() As String
        ReDim arrayValues(7)
        Dim strError As String

        strError = ""

        If C1FlexGrid_DataCollector.Row <= 1 Then
            MsgBox("First you must select one row")
            Exit Sub
        End If


        Select Case Me.ComboBox_FieldBook.SelectedIndex
            Case 2
                PopulateArray(C1FlexGrid_DataCollector(C1FlexGrid_DataCollector.Row, 0).ToString(), arrayValues)
                PrintLabel(arrayValues, "", Me.ComboBox_Port.SelectedItem.ToString, "Test", strError)
            Case 1
                arrayValues(0) = C1FlexGrid_DataCollector(C1FlexGrid_DataCollector.Row, 2).ToString()
                arrayValues(1) = C1FlexGrid_DataCollector(C1FlexGrid_DataCollector.Row, 4).ToString()
                arrayValues(2) = C1FlexGrid_DataCollector(C1FlexGrid_DataCollector.Row, 6).ToString()
                'arrayValues(3) = C1FlexGrid_DataCollector(C1FlexGrid_DataCollector.Row, 5).ToString()
                'testing
                Dim stream As FileStream = File.Open(Me.OpenFileDialog1.FileName, FileMode.Open, FileAccess.Read)
                Dim excelReader As IExcelDataReader
                If Path.GetExtension(Me.TextBoxFileName.Text).ToLower().Equals(".xlsx") Then
                    excelReader = ExcelReaderFactory.CreateOpenXmlReader(stream)
                ElseIf Path.GetExtension(Me.TextBoxFileName.Text).ToLower().Equals(".xls") Then
                    excelReader = ExcelReaderFactory.CreateBinaryReader(stream)
                End If
                excelReader.NextResult()
                excelReader.NextResult()
                excelReader.NextResult()
                excelReader.NextResult()
                Dim ExcelRow As Integer = 0
                While excelReader.Read()
                    If ExcelRow = 11 Then 'If  Then
                        arrayValues(3) = excelReader.GetString(1)
                        Exit While
                    End If
                    ExcelRow += 1
                End While
                excelReader.Close()
                PrintLabel(arrayValues, "", Me.ComboBox_Port.SelectedItem.ToString, "Test2", strError)
            Case 0
                For indexRow As Integer = C1FlexGrid_DataCollector.Selection.TopRow To C1FlexGrid_DataCollector.Selection.BottomRow
                    arrayValues(1) = C1FlexGrid_DataCollector(indexRow, 0).ToString() 'Plot No
                    arrayValues(2) = C1FlexGrid_DataCollector(indexRow, 4).ToString() 'Genotype
                    arrayValues(3) = C1FlexGrid_DataCollector(indexRow, 1).ToString() 'Rep. No
                    arrayValues(4) = "Harvest" 'Activity
                    arrayValues(5) = Date.Today.Day.ToString() + "." + Date.Today.Month.ToString() + "." + Date.Today.Year.ToString() 'Date
                    Dim stream As FileStream = File.Open(Me.OpenFileDialog1.FileName, FileMode.Open, FileAccess.Read)
                    Dim excelReader As IExcelDataReader = Nothing
                    If Path.GetExtension(Me.TextBoxFileName.Text).ToLower().Equals(".xlsx") Then
                        excelReader = ExcelReaderFactory.CreateOpenXmlReader(stream)
                    ElseIf Path.GetExtension(Me.TextBoxFileName.Text).ToLower().Equals(".xls") Then
                        excelReader = ExcelReaderFactory.CreateBinaryReader(stream)
                    End If
                    Dim ExcelRow As Integer
                    ExcelRow = 0
                    While excelReader.Read()
                        If ExcelRow = 16 Then
                            arrayValues(0) = excelReader.GetString(7) 'Trial name
                            Exit While
                        End If
                        ExcelRow += 1
                    End While
                    'excelReader.NextResult()
                    excelReader.NextResult()
                    excelReader.NextResult()
                    excelReader.NextResult()
                    ExcelRow = 0
                    While excelReader.Read()
                        If ExcelRow = 11 Then
                            arrayValues(6) = excelReader.GetString(1) 'Site name
                            Exit While
                        End If
                        ExcelRow += 1
                    End While
                    excelReader.Close()
                    PrintLabel(arrayValues, "", Me.ComboBox_Port.SelectedItem.ToString, "CloneSelector1", strError)
                Next
        End Select
    End Sub

    Private Sub TextBox_barcode_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles TextBox_barcode.KeyPress
        Dim strBarcode As String

        If e.KeyChar = Chr(13) Then
            strBarcode = Me.TextBox_barcode.Text

            'Parse Barcode
            Dim BarcodeValues As String()
            Dim flag As Boolean = False

            If Me.ListBoxToCompare.Items.Count = 0 Then
                MsgBox("There're not any columns selected to compare with the scanned barcode")
            ElseIf Me.ListBoxToCompare.Items.Count > 1 And TextBoxCharConcat.Text = "" Then
                MsgBox("The number of selected columns to compare are more than 1 and the concatenation character is not defined")
            ElseIf Me.ListBoxToCompare.Items.Count = 1 Then
                Dim indRow As Integer
                For indRow = 0 To C1FlexGrid_DataCollector.Rows.Count - 1
                    Dim indColumn As Integer = Integer.Parse(ListBoxToCompare.Items(0).ToString().Split("|")(0))
                    If C1FlexGrid_DataCollector(indRow, indColumn) = strBarcode Then
                        C1FlexGrid_DataCollector.Focus()
                        C1FlexGrid_DataCollector.Select(indRow, C1FlexGrid_DataCollector.Cols.Frozen)
                        flag = True
                        Exit For
                    End If
                Next
                If flag = False Then
                    MsgBox("The barcode value has not been found")
                End If

            ElseIf Me.ListBoxToCompare.Items.Count > 1 Then
                BarcodeValues = strBarcode.Split(TextBoxCharConcat.Text)

                If BarcodeValues.Length <> ListBoxToCompare.Items.Count Then
                    MsgBox("The number of values in the scanned barcode is different to the number of selected columns to compare")
                Else
                    Dim indRow, indBarcode As Integer
                    For indRow = 0 To C1FlexGrid_DataCollector.Rows.Count - 1
                        flag = True
                        Dim indColumn As Integer
                        For indBarcode = 0 To BarcodeValues.Length - 1
                            indColumn = Integer.Parse(ListBoxToCompare.Items(indBarcode).ToString().Split("|")(0))
                            If BarcodeValues(indBarcode) <> C1FlexGrid_DataCollector(indRow, indColumn) Then
                                flag = False
                                Exit For
                            End If
                        Next
                        If flag = True Then
                            C1FlexGrid_DataCollector.Select(indRow, indColumn)
                            Exit For
                        End If
                    Next
                    If flag = False Then
                        MsgBox("The barcode value has not been found")
                    End If
                End If
            End If
            TextBox_barcode.SelectAll()
        End If
    End Sub

    Private Sub ButtonAdd_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button_Add.Click
        If ListBoxColumns.SelectedIndex > -1 Then
            ListBoxToCompare.Items.Add(ListBoxColumns.SelectedItem.ToString())
            ListBoxColumns.Items.RemoveAt(ListBoxColumns.SelectedIndex)
        End If
    End Sub

    Private Sub ButtonDel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button_Del.Click
        If ListBoxToCompare.SelectedIndex > -1 Then
            ListBoxColumns.Items.Add(ListBoxToCompare.SelectedItem.ToString())
            ListBoxToCompare.Items.RemoveAt(ListBoxToCompare.SelectedIndex)
        End If
    End Sub

    Private Sub ButtonBuscar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button_Search.Click
        Dim strBarcode As String
        strBarcode = Me.TextBox_barcode.Text
        'Parse Barcode
        Dim BarcodeValues As String()
        Dim flag As Boolean = False

        If Me.ListBoxToCompare.Items.Count = 0 Then
            MsgBox("There're not any columns selected to compare with the scanned barcode")
        ElseIf Me.ListBoxToCompare.Items.Count > 1 And TextBoxCharConcat.Text = "" Then
            MsgBox("The number of selected columns to compare are more than 1 and the concatenation character is not defined")
        ElseIf Me.ListBoxToCompare.Items.Count = 1 Then
            Dim indRow As Integer
            For indRow = 0 To C1FlexGrid_DataCollector.Rows.Count - 1
                Dim indColumn As Integer = Integer.Parse(ListBoxToCompare.Items(0).ToString().Split("|")(0))
                If C1FlexGrid_DataCollector(indRow, indColumn) = strBarcode Then
                    C1FlexGrid_DataCollector.Focus()
                    C1FlexGrid_DataCollector.Select(indRow, C1FlexGrid_DataCollector.Cols.Frozen)
                    flag = True
                    Exit For
                End If
            Next
            If flag = False Then
                MsgBox("The barcode value has not been found")
            End If

        ElseIf Me.ListBoxToCompare.Items.Count > 1 Then
            BarcodeValues = strBarcode.Split(TextBoxCharConcat.Text)

            If BarcodeValues.Length <> ListBoxToCompare.Items.Count Then
                MsgBox("The number of values in the scanned barcode is different to the number of selected columns to compare")
            Else
                Dim indRow, indBarcode As Integer
                For indRow = 0 To C1FlexGrid_DataCollector.Rows.Count - 1
                    flag = True
                    Dim indColumn As Integer
                    For indBarcode = 0 To BarcodeValues.Length - 1
                        indColumn = Integer.Parse(ListBoxToCompare.Items(indBarcode).ToString().Split("|")(0))
                        If BarcodeValues(indBarcode) <> C1FlexGrid_DataCollector(indRow, indColumn) Then
                            flag = False
                            Exit For
                        End If
                    Next
                    If flag = True Then
                        C1FlexGrid_DataCollector.Select(indRow, indColumn)
                        Exit For
                    End If
                Next
                If flag = False Then
                    MsgBox("The barcode value has not been found")
                End If
            End If
        End If
        TextBox_barcode.SelectAll()


        'Dim stream As FileStream = File.Open(Me.OpenFileDialog1.FileName, FileMode.Open, FileAccess.Read)
        'Dim excelReader As IExcelDataReader
        'If Path.GetExtension(Me.TextBoxFieldBook.Text).ToLower().Equals(".xlsx") Then
        '    excelReader = ExcelReaderFactory.CreateOpenXmlReader(stream)
        'ElseIf Path.GetExtension(Me.TextBoxFieldBook.Text).ToLower().Equals(".xls") Then
        '    excelReader = ExcelReaderFactory.CreateBinaryReader(stream)
        'End If
        'Dim ExcelRow As Integer
        'ExcelRow = 0
        'While excelReader.Read()
        '    If ExcelRow = 16 Then
        '        MsgBox(excelReader.GetString(7))
        '        Exit While
        '    End If
        '    ExcelRow += 1
        'End While
        'excelReader.NextResult()
        'excelReader.NextResult()
        'excelReader.NextResult()
        'ExcelRow = 0
        'While excelReader.Read()
        '    If ExcelRow = 11 Then
        '        MsgBox(excelReader.GetString(1))
        '        Exit While
        '    End If
        '    ExcelRow += 1
        'End While
        'excelReader.Close()
    End Sub

    Private Sub C1FlexGrid_DataCollector_RowColChange(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles C1FlexGrid_DataCollector.RowColChange
        If ComboBox_FieldBook.SelectedIndex = 0 Then
            Try
                Label_GenotypeName.Text = C1FlexGrid_DataCollector(C1FlexGrid_DataCollector.Row, 4).ToString()
                Label_Rep.Text = C1FlexGrid_DataCollector(C1FlexGrid_DataCollector.Row, 1).ToString()
            Catch ex As Exception
                'colmuns 4 and 1 dont exist
            End Try
        End If
    End Sub

    Private Sub MenuItemCopy_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MenuItemCopy.Click

        If C1FlexGrid_DataCollector.Row < 2 Then
            Call ShowSystemMessage("Select a cell")
            Return
        End If

        Dim clipboardValue As Object = C1FlexGrid_DataCollector(C1FlexGrid_DataCollector.Row, C1FlexGrid_DataCollector.Col)

        If clipboardValue Is Nothing Then
            Clipboard.SetDataObject("")
        Else
            Clipboard.SetDataObject(clipboardValue.ToString)
        End If
    End Sub

    Private Sub MenuItemPaste_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MenuItemPaste.Click

        If C1FlexGrid_DataCollector.Row < 2 Then
            Call ShowSystemMessage("Select a cell")
            Return
        End If

        Dim iDataObj As IDataObject = Clipboard.GetDataObject()
        Dim clipboardValue As String = ""

        If iDataObj Is Nothing Then
            C1FlexGrid_DataCollector(C1FlexGrid_DataCollector.Row, C1FlexGrid_DataCollector.Col) = ""
        Else
            clipboardValue = iDataObj.GetData("".GetType())

            Dim ok As Boolean
            Dim Message As String
            Dim row As Integer = C1FlexGrid_DataCollector.Row
            Dim col As Integer = C1FlexGrid_DataCollector.Col
            ok = True
            Message = ""
            Dim CellFormat = C1FlexGrid_DataCollector(1, col)

            If CellFormat = "" Then
                ok = True

            ElseIf CellFormat Is Nothing Then
                If clipboardValue.Length > 255 Then
                    Message = "Error: The maximum length of the value is 255."
                    ok = False
                End If

            ElseIf CellFormat.ToString.ToUpper() = "INT" Then
                Dim IntValue As Integer
                Try
                    IntValue = Integer.Parse(clipboardValue)
                Catch ex As Exception
                    Message = ("Error: The value must be an integer")
                    ok = False
                End Try

            ElseIf CellFormat.ToString().ToUpper() = "DATE" Then
                Dim DateValue As Date
                Try
                    DateValue = Date.ParseExact(clipboardValue, "dd/mm/yyyy", Globalization.CultureInfo.InvariantCulture)
                Catch
                    Message = ("Error: The value must be a date")
                    ok = False
                End Try

            ElseIf CellFormat.ToString().ToUpper().StartsWith("STRING") Then
                Dim cellVals As String()
                cellVals = CellFormat.ToString().Split(":")
                Dim lenMax As Integer
                lenMax = Integer.Parse(cellVals(1))
                If clipboardValue.Length > lenMax Then
                    Message = ("Error: El dato puede tener como m�ximo " + lenMax.ToString() + " caracteres.")
                    ok = False
                End If

            ElseIf CellFormat.ToString() = "" Then
                If clipboardValue.Length > 255 Then
                    Message = ("Error: El dato puede tener como m�ximo 255 caracteres.")
                    ok = False
                End If

            ElseIf CellFormat.ToString().ToUpper().StartsWith("RANGE") Then
                Dim cellVals As String()
                cellVals = CellFormat.ToString().Split(":")(1).Split("-")
                Dim LowerLimit, UpperLimit, value As Integer
                LowerLimit = Integer.Parse(cellVals(0))
                UpperLimit = Integer.Parse(cellVals(1))

                Try
                    value = Integer.Parse(clipboardValue)
                    If value < LowerLimit Or value > UpperLimit Then
                        Message = ("Error : The value must be between " + LowerLimit.ToString() + " and " + UpperLimit.ToString())
                        ok = False
                    End If
                Catch ex As Exception
                    Message = ("Error: The value must be an integer")
                    ok = False
                End Try

            ElseIf CellFormat.ToString().ToUpper().StartsWith("MORETHANEQUAL") Then
                Dim cellVals As String()
                cellVals = CellFormat.ToString().Split(":")
                Dim LowerLimit, value As Integer
                LowerLimit = Integer.Parse(cellVals(1))

                Try
                    value = Integer.Parse(clipboardValue)
                    If value < LowerLimit Then
                        Message = ("Error: The value can't be less than " + LowerLimit.ToString())
                        ok = False
                    End If
                Catch ex As Exception
                    Message = ("Error: The value must be an integer")
                    ok = False
                End Try

            End If

            If ok Then
                C1FlexGrid_DataCollector(row, col) = clipboardValue
                ButtonSaveFieldBook_Click(Nothing, Nothing)
            Else
                Call ShowSystemMessage(Message)
            End If
        End If

    End Sub

    Private Sub MenuItemClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MenuItemClose.Click
        StopRead()
        Me.Close()
    End Sub
    Private Sub TabControl_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles TabControl.SelectedIndexChanged
        If TabControl.SelectedIndex = 1 Then
            C1FlexGrid_DataCollector.Focus()
        End If
    End Sub
    Private Sub C1FlexGrid_DataCollector_KeyPressEdit(ByVal sender As System.Object, ByVal e As C1.Win.C1FlexGrid.KeyPressEditEventArgs) Handles C1FlexGrid_DataCollector.KeyPressEdit
        If e.KeyChar = vbCr Then
            EnterKeyEdit = True
            C1FlexGrid_DataCollector.Focus()
        End If
    End Sub
    Private Sub C1FlexGrid_DataCollector_StartEdit(ByVal sender As System.Object, ByVal e As C1.Win.C1FlexGrid.RowColEventArgs) Handles C1FlexGrid_DataCollector.StartEdit
        If Not EvalError Then
            currentValue = C1FlexGrid_DataCollector(e.Row, e.Col).ToString()
        End If
        If CheckBox_InputForm.Checked Then
            e.Cancel = True

            'Redim RowsCount
            C1FlexGrid_InputForm.Rows.Count = C1FlexGrid_DataCollector.Cols.Count

            C1FlexGrid_InputForm.Redraw = False

            For iCol As Integer = 0 To C1FlexGrid_DataCollector.Cols.Count - 1
                C1FlexGrid_InputForm(iCol, 0) = C1FlexGrid_DataCollector(0, iCol)
                C1FlexGrid_InputForm(iCol, 1) = C1FlexGrid_DataCollector(1, iCol)
                C1FlexGrid_InputForm(iCol, 2) = C1FlexGrid_DataCollector(e.Row, iCol)



                If Not (C1FlexGrid_InputForm(iCol, 1) Is DBNull.Value Or C1FlexGrid_InputForm(iCol, 1) Is Nothing) Then
                    If C1FlexGrid_InputForm(iCol, 1).ToString().ToUpper().StartsWith("CATEG:") Then
                        Dim ComboItems As String() = C1FlexGrid_InputForm(iCol, 1).ToString().Split(":")
                        C1FlexGrid_InputForm.Rows(iCol).ComboList = " |" + ComboItems(1)
                    End If
                End If
            Next

            C1FlexGrid_InputForm.Cols(1).Visible = False

            'C1FlexGrid_InputForm.Rows().Move(4, 1)
            'C1FlexGrid_InputForm.Rows().Move(2, 1)

            C1FlexGrid_InputForm.Cols.Fixed = 1
            C1FlexGrid_InputForm.Rows.Frozen = NumericUpDownFrozenRowsNumberFom.Value

            C1FlexGrid_InputForm.Cols(0).Width = Panel_InputForm.Width / 2 - 15
            C1FlexGrid_InputForm.Cols(2).Width = Panel_InputForm.Width / 2 - 15
            C1FlexGrid_InputForm.Select(e.Col, 2)

            C1FlexGrid_InputForm.Redraw = True
            C1FlexGrid_InputForm.Focus()

            Panel_InputForm.Left = 0
            Panel_InputForm.Top = 0
            Panel_InputForm.Visible = True

            '
            C1FlexGrid_DataCollector.Enabled = False
            Button_Print.Enabled = False
            Button_Save.Enabled = False
            Button_Search.Enabled = False
            TextBox_barcode.Enabled = False
        End If
    End Sub

    Private Sub Button_InputFormCancel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button_InputFormCancel.Click
        '
        C1FlexGrid_DataCollector.Enabled = True
        Button_Print.Enabled = True
        Button_Save.Enabled = True
        Button_Search.Enabled = True
        TextBox_barcode.Enabled = True

        Panel_InputForm.Visible = False
        C1FlexGrid_DataCollector.Focus()
    End Sub

    Private Sub Button_InputFormOK_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button_InputFormOK.Click
        C1FlexGrid_DataCollector.Enabled = True
        Button_Print.Enabled = True
        Button_Save.Enabled = True
        Button_Search.Enabled = True
        TextBox_barcode.Enabled = True

        C1FlexGrid_InputForm.Focus()
        If EvalError = False Then
            Dim result As Object
            If (CheckBox_ReplaceBlankValue.Checked = True) Then
                result = DialogResult.Yes
            Else
                result = DialogResult.No
                'result = MessageBox.Show("Replace blank values?", "Y/N", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button1)
            End If
            If (result = DialogResult.No) Then
                For iCol As Integer = 0 To C1FlexGrid_DataCollector.Cols.Count - 1
                    C1FlexGrid_DataCollector(C1FlexGrid_DataCollector.RowSel, iCol) = C1FlexGrid_InputForm(iCol, 2)
                Next
            Else
                For iCol As Integer = 0 To C1FlexGrid_DataCollector.Cols.Count - 1
                    Dim gridObj = C1FlexGrid_InputForm(iCol, 2)
                    If C1FlexGrid_InputForm(iCol, 2) Is Nothing Then
                        C1FlexGrid_DataCollector(C1FlexGrid_DataCollector.RowSel, iCol) = TextBox_BlankValue.Text.Trim
                    ElseIf C1FlexGrid_InputForm(iCol, 2).ToString = "" Then
                        C1FlexGrid_DataCollector(C1FlexGrid_DataCollector.RowSel, iCol) = TextBox_BlankValue.Text.Trim
                    Else
                        C1FlexGrid_DataCollector(C1FlexGrid_DataCollector.RowSel, iCol) = C1FlexGrid_InputForm(iCol, 2)
                    End If
                Next
            End If

            Panel_InputForm.Visible = False
            C1FlexGrid_DataCollector.Focus()
            ButtonSaveFieldBook_Click(Nothing, Nothing)
        End If
    End Sub

    Private Sub Validate_Cells(ByVal CellValue As System.Object, ByVal CellFormat As System.Object, ByRef Message As String)
        If CellValue.ToString() = "" Then
            EvalError = False
            '<NOTHING>
        ElseIf CellFormat Is Nothing Then
            If CellValue.ToString().Length > 255 Then
                Message = translate("Str_ErrorString", "Error: The maximum length of the value is 255.")
                EvalError = True
            End If
            '<EMPTY STRING>
        ElseIf CellFormat.ToString() = "" Then
            If CellValue.ToString().Length > 255 Then
                Message = translate("Str_ErrorString", "Error: The maximum length of the value is 255.")
                EvalError = True
            End If
            'STRING
        ElseIf CellFormat.ToString.ToUpper() = "STRING" Then
            If CellValue.ToString().Length > 255 Then
                Message = translate("Str_ErrorString", "Error: The maximum length of the value is 255.")
                EvalError = True
            End If
            'STRING:<maximum length>
        ElseIf CellFormat.ToString().ToUpper().StartsWith("STRING:") Then
            Dim cellVals As String()
            cellVals = CellFormat.ToString().Split(":")
            Dim lenMax As Integer
            lenMax = Integer.Parse(cellVals(1))
            If CellValue.ToString().Length > lenMax Then
                Message = String.Format(translate("Str_ErrorStringLength", "Error: The maximum length of the value is {0}"), lenMax.ToString())
                EvalError = True
            End If
        ElseIf CellFormat.ToString().ToUpper().StartsWith("STR_FIX:") Then
            Dim cellVals As String()
            cellVals = CellFormat.ToString().Split(":")
            Dim lenFix As Integer
            lenFix = Integer.Parse(cellVals(1))
            If CellValue.ToString().Length <> lenFix Then
                Message = String.Format(translate("Str_ErrorStringLength", "Error: The length of the value should be {0} characters"), lenFix.ToString())
                EvalError = True
            End If
            'INTEGER
        ElseIf CellFormat.ToString.ToUpper() = "INT" Then
            Dim IntValue As Integer
            Try
                IntValue = Integer.Parse(CellValue)
            Catch ex As Exception
                Message = translate("Str_ErrorInteger", "Error: The value must be an integer")
                EvalError = True
            End Try
            'DECIMAL
        ElseIf CellFormat.ToString.ToUpper() = "DECIMAL" Then
            If IsNumeric(CellValue) = False Then
                Message = translate("Str_ErrorDecimal", "Error: The value must be numeric")
                EvalError = True
            End If
            'DECIMAL:<maximum length>
        ElseIf CellFormat.ToString().ToUpper().StartsWith("DECIMAL:") Then
            If IsNumeric(CellValue) = False Then
                Message = translate("Str_ErrorDecimal", "Error: The value must be numeric")
                EvalError = True
            End If
            Dim cellVals As String()
            Dim lenMax As Integer
            Dim inputVals As String()
            cellVals = CellFormat.ToString().Split(":")
            lenMax = Integer.Parse(cellVals(1))
            inputVals = CellValue.ToString().Split(".")
            If inputVals.Length < 2 Then
                EvalError = True
            Else
                If inputVals(1).Length <> lenMax Then
                    EvalError = True
                End If
            End If
            If EvalError Then
                Message = String.Format(translate("Str_ErrorDecimal", "Error: The number of decimals should be exactly {0} "), lenMax.ToString())
            End If

            'DATE
        ElseIf CellFormat.ToString().ToUpper() = "DATE" Then
            Dim DateValue As Date
            Try
                DateValue = Date.ParseExact(CellValue, "dd/MM/yyyy", Globalization.CultureInfo.InvariantCulture)
            Catch
                Message = translate("Str_ErrorDate", "Error: The value must be a date with this format dd/MM/yyyy")
                EvalError = True
            End Try
            'RANGE:<Lower Limit Integer>-<Upper Limit Integer>
        ElseIf CellFormat.ToString().ToUpper().StartsWith("RANGE:") Then
            Dim cellVals As String()
            cellVals = CellFormat.ToString().Split(":")(1).Split("-")
            Dim LowerLimit, UpperLimit, value As Integer
            LowerLimit = Integer.Parse(cellVals(0))
            UpperLimit = Integer.Parse(cellVals(1))
            Try
                value = Integer.Parse(CellValue)
                If value < LowerLimit Or value > UpperLimit Then
                    Message = String.Format(translate("Str_ErrorIntegerRange", "Error : The value must be between {0} and {1}"), LowerLimit.ToString(), UpperLimit.ToString())
                    EvalError = True
                End If
            Catch ex As Exception
                Message = translate("Str_ErrorInteger", "Error: The value must be an integer")
                EvalError = True
            End Try
            'MORETHANEQUAL:<Lower Limit Integer>
        ElseIf CellFormat.ToString().ToUpper().StartsWith("MORETHANEQUAL:") Then
            Dim cellVals As String()
            cellVals = CellFormat.ToString().Split(":")
            Dim LowerLimit, value As Integer
            LowerLimit = Integer.Parse(cellVals(1))
            Try
                value = Integer.Parse(CellValue)
                If value < LowerLimit Then
                    Message = String.Format(translate("Str_ErrorIntegerMoreThanEqual", "Error: The value can't be less than {0}"), LowerLimit.ToString())
                    EvalError = True
                End If
            Catch ex As Exception
                Message = translate("Str_ErrorInteger", "Error: The value must be an integer")
                EvalError = True
            End Try
            'DEC_RANGE:<Lower Limit Decimal>-<Upper Limit Decimal>
        ElseIf CellFormat.ToString().ToUpper().StartsWith("DEC_RANGE:") Then
            Dim cellVals As String()
            cellVals = CellFormat.ToString().Split(":")(1).Split("-")
            Dim LowerLimit, UpperLimit, value As Double
            LowerLimit = Double.Parse(cellVals(0))
            UpperLimit = Double.Parse(cellVals(1))
            Try
                value = Double.Parse(CellValue)
                If value < LowerLimit Or value > UpperLimit Then
                    Message = String.Format(translate("Str_ErrorDecimalRange", "Error : The value must be between {0} and {1}"), LowerLimit.ToString(), UpperLimit.ToString())
                    EvalError = True
                End If
            Catch ex As Exception
                Message = translate("Str_ErrorDecimal", "Error: The value must be numeric")
                EvalError = True
            End Try
            'ODD
        ElseIf CellFormat.ToString.ToUpper().StartsWith("ODD_RANGE:") Then
            Dim IntValue As Integer
            Try
                IntValue = Integer.Parse(CellValue)
            Catch ex As Exception
                Message = translate("Str_ErrorInteger", "Error: The value must be an integer")
                EvalError = True
                Exit Sub
            End Try
            Dim cellVals As String()
            cellVals = CellFormat.ToString().Split(":")(1).Split("-")
            Dim LowerLimit, UpperLimit, value As Integer
            LowerLimit = Integer.Parse(cellVals(0))
            UpperLimit = Integer.Parse(cellVals(1))
            If IntValue Mod 2 <> 0 Then
                If IntValue < LowerLimit Or IntValue > UpperLimit Then
                    Message = String.Format(translate("Str_ErrorInteger", "Error : The value must be an odd number between {0} and {1}."), LowerLimit.ToString(), UpperLimit.ToString())
                    EvalError = True
                End If
            Else
                Message = translate("Str_ErrorInteger", "Error : The value must be an odd number")
                EvalError = True
            End If
        End If
    End Sub

    Private Sub C1FlexGrid_InputForm_AfterEdit(ByVal sender As System.Object, ByVal e As C1.Win.C1FlexGrid.RowColEventArgs) Handles C1FlexGrid_InputForm.AfterEdit
        EvalError = False

        Dim Message As String = ""
        Dim CellValue As Object = C1FlexGrid_InputForm(e.Row, e.Col)
        Dim CellFormat As Object = C1FlexGrid_InputForm(e.Row, 1)

        Validate_Cells(CellValue, CellFormat, Message)

        If EvalError = True Then
            'Desactivar o activar otros controles
            Button_Save.Enabled = False
            Panel_Message.Visible = True
            Panel_Message.Left = 0
            Panel_Message.Top = C1FlexGrid_InputForm.Bottom
            Panel_Message.Height = TabPage_CollectData.Height - C1FlexGrid_InputForm.Bottom
            Panel_Message.Width = Me.Width
            Label_Message.Text = Message

            e.Cancel = True

            C1FlexGrid_InputForm.Focus()
            C1FlexGrid_InputForm.StartEditing(e.Row, e.Col)
        Else
            If chkNotif.Checked Then
                If currentValue <> C1FlexGrid_InputForm(e.Row, e.Col).ToString() Then
                    Dim LResponse As Integer
                    LResponse = MsgBox("Are you sure that you want to change the cell value?", vbYesNo)
                    If LResponse = vbNo Then
                        C1FlexGrid_InputForm(e.Row, e.Col) = currentValue
                    End If
                End If
            End If

            Button_Save.Enabled = True
            Panel_Message.Visible = False

            C1FlexGrid_InputForm.Focus()


            If EnterKeyEdit = True Then
                If e.Row < C1FlexGrid_InputForm.Rows.Count - 1 Then
                    C1FlexGrid_InputForm.Select(e.Row + 1, 2)
                End If
                EnterKeyEdit = False
            End If

        End If
        EnterKeyEdit = False
    End Sub
    Private Sub C1FlexGrid_InputForm_KeyPressEdit(ByVal sender As System.Object, ByVal e As C1.Win.C1FlexGrid.KeyPressEditEventArgs) Handles C1FlexGrid_InputForm.KeyPressEdit
        If e.KeyChar = Chr(13) Then
            EnterKeyEdit = True
            C1FlexGrid_InputForm.Focus()
        End If
    End Sub
#Region "OptionsTab Events"
    Private Sub CheckBox_Actived_CheckStateChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles CheckBox_InputForm.CheckStateChanged
        If (CheckBox_InputForm.Checked = False) Then
            translateControl(CheckBox_InputForm, "Str_NO", "NO")
            NumericUpDownFrozenRowsNumberFom.Enabled = False
            myset.ListItems.Set(CheckBox_InputForm.Name, "0")
        Else
            translateControl(CheckBox_InputForm, "Str_YES", "YES")
            NumericUpDownFrozenRowsNumberFom.Enabled = True
            myset.ListItems.Set(CheckBox_InputForm.Name, "1")
        End If
        myset.SaveXmlCurrentConfiguration()
    End Sub
    Private Sub ComboBox_Language_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ComboBox_Language.SelectedIndexChanged
        myset.ListItems.Set(ComboBox_Language.Name, Str(ComboBox_Language.SelectedIndex))
        myset.SaveXmlCurrentConfiguration()

        updateControls(Me)
    End Sub

    Private Sub updateControls(ByVal control As Control)

        If TypeOf control Is Label Or TypeOf control Is Button Or TypeOf control Is TabPage Then
            Try
                control.Text = myManager.GetString(control.Name, New System.Globalization.CultureInfo(ComboBox_Language.SelectedValue.ToString, True))

                Select Case ComboBox_Language.SelectedValue.ToString
                    Case "uz"
                        control.Font = New System.Drawing.Font("MingLiU", control.Font.Size, control.Font.Style)
                    Case Else
                        'If control.Name = Label_OptionsTitle.Name Or control.Name = Label_CollectDataTitle.Name _
                        '    Or control.Name = Label_CIP.Name Then
                        '    control.Font = New System.Drawing.Font("Tahoma", 10.0!, FontStyle.Bold)
                        'Else
                        control.Font = New System.Drawing.Font("Tahoma", control.Font.Size, control.Font.Style)
                        'End If
                End Select
            Catch e As Exception
            End Try
        ElseIf TypeOf control Is TabControl Then
            Try
                Select Case ComboBox_Language.SelectedValue.ToString
                    Case "uz"
                        control.Font = New System.Drawing.Font("MingLiU", 10.0!, FontStyle.Bold)
                    Case Else
                        control.Font = New System.Drawing.Font("Tahoma", 9.0!, FontStyle.Regular)
                End Select
            Catch ex As Exception
            End Try
        End If

        For Each c As Control In control.Controls
            updateControls(c)
        Next
    End Sub

    Private Sub NumericUpDownFrozenColsNumber_LostFocus(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles NumericUpDownFrozenColsNumber.LostFocus
        Try
            If NumericUpDownFrozenColsNumber.Value < C1FlexGrid_DataCollector.Cols.Count Then
                C1FlexGrid_DataCollector.Cols.Frozen = NumericUpDownFrozenColsNumber.Value
                For col As Integer = 0 To NumericUpDownFrozenColsNumber.Value - 1
                    C1FlexGrid_DataCollector.Cols(col).AllowEditing = False
                Next
                For col As Integer = NumericUpDownFrozenColsNumber.Value To C1FlexGrid_DataCollector.Cols.Count - 1
                    C1FlexGrid_DataCollector.Cols(col).AllowEditing = True
                Next
            End If

            myset.ListItems.Set(NumericUpDownFrozenColsNumber.Name, Str(NumericUpDownFrozenColsNumber.Value))
            myset.SaveXmlCurrentConfiguration()
        Catch ex As Exception
        End Try
    End Sub

    Private Sub NumericUpDownFrozenRowsNumberFom_LostFocus(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles NumericUpDownFrozenRowsNumberFom.LostFocus
        Try
            C1FlexGrid_InputForm.Rows.Frozen = NumericUpDownFrozenRowsNumberFom.Value
            myset.ListItems.Set(NumericUpDownFrozenRowsNumberFom.Name, Str(NumericUpDownFrozenRowsNumberFom.Value))
            myset.SaveXmlCurrentConfiguration()
        Catch ex As Exception
        End Try
    End Sub
    Private Sub ComboBox_FieldBook_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ComboBox_FieldBook.SelectedIndexChanged
        myset.ListItems.Set(ComboBox_FieldBook.Name, Str(ComboBox_FieldBook.SelectedIndex))
        myset.SaveXmlCurrentConfiguration()
    End Sub
    Private Sub ComboBox_Port_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ComboBox_Port.SelectedIndexChanged
        myset.ListItems.Set(ComboBox_Port.Name, Str(ComboBox_Port.SelectedIndex))
        myset.SaveXmlCurrentConfiguration()
    End Sub

    Private Sub ComboBox_LabelType_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ComboBox_LabelType.SelectedIndexChanged
        myset.ListItems.Set(ComboBox_LabelType.Name, Str(ComboBox_LabelType.SelectedIndex))
        myset.SaveXmlCurrentConfiguration()
    End Sub

    Private Sub CheckBox_ReplaceBlankValue_CheckStateChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles CheckBox_ReplaceBlankValue.CheckStateChanged
        If CheckBox_ReplaceBlankValue.Checked Then
            translateControl(CheckBox_ReplaceBlankValue, "Str_YES", "YES")
            TextBox_BlankValue.Enabled = True
            myset.ListItems.Set(CheckBox_ReplaceBlankValue.Name, "1")
        Else
            translateControl(CheckBox_ReplaceBlankValue, "Str_NO", "NO")
            TextBox_BlankValue.Enabled = False
            myset.ListItems.Set(CheckBox_ReplaceBlankValue.Name, "0")
        End If

        myset.SaveXmlCurrentConfiguration()
    End Sub

    Private Sub TextBox_BlankValue_LostFocus(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles TextBox_BlankValue.LostFocus
        myset.ListItems.Set(TextBox_BlankValue.Name, TextBox_BlankValue.Text.Trim)

        myset.SaveXmlCurrentConfiguration()
    End Sub
#End Region

    Private Function translate(ByVal NameResource As String, ByVal DefaultValue As String) As String
        Dim lang_value = ""
        Try
            lang_value = myManager.GetString(NameResource, New System.Globalization.CultureInfo(ComboBox_Language.SelectedValue.ToString, True))
        Catch ex As Exception
            lang_value = DefaultValue
        End Try
        Return lang_value
    End Function
    Private Sub translateControl(ByVal control As Control, ByVal NameResource As String, ByVal DefaultValue As String)
        Try
            control.Text = myManager.GetString(NameResource, New System.Globalization.CultureInfo(ComboBox_Language.SelectedValue.ToString, True))
            Select Case ComboBox_Language.SelectedValue.ToString
                Case "uz"
                    control.Font = New System.Drawing.Font("MingLiU", 10.0!, FontStyle.Bold)
                Case Else
                    control.Font = New System.Drawing.Font("Tahoma", 9.0!, FontStyle.Regular)
            End Select
        Catch ex As Exception
            control.Text = DefaultValue
            control.Font = New System.Drawing.Font("Tahoma", 9.0!, FontStyle.Regular)
        End Try
    End Sub

    Private Sub CheckBox_Autosave_CheckStateChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles CheckBox_Autosave.CheckStateChanged
        If CheckBox_Autosave.Checked Then
            translateControl(CheckBox_Autosave, "Str_YES", "YES")
            myset.ListItems.Set(CheckBox_Autosave.Name, "1")
        Else
            translateControl(CheckBox_Autosave, "Str_NO", "NO")
            myset.ListItems.Set(CheckBox_Autosave.Name, "0")
        End If

        myset.SaveXmlCurrentConfiguration()
    End Sub


    Private Sub Button_SFFormCancel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button_SFFormCancel.Click
        Panel_SavedFiles.Top = 0
        Panel_SavedFiles.Left = Me.Width
        Panel_SavedFiles.Visible = False
    End Sub

    Private Sub Button_SFFormLoad_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button_SFFormLoad.Click

        If ListView_SavedFiles.SelectedIndices.Count > 0 Then
            Dim detailsNameRead As String = Path.GetDirectoryName(curFile) + "\" + ListView_SavedFiles.Items(ListView_SavedFiles.FocusedItem.Index).Text
            Dim objReader As New System.IO.StreamReader(detailsNameRead)

            Dim iRow As Integer = 0
            Do While objReader.Peek() >= 0
                Dim line As String = objReader.ReadLine()
                Dim data As String() = line.Split(Chr(9))
                For iCol As Integer = 0 To data.Length - 1
                    If iRow < C1FlexGrid_DataCollector.Rows.Count And iCol < C1FlexGrid_DataCollector.Cols.Count Then
                        C1FlexGrid_DataCollector(iRow, iCol) = data(iCol)
                        'MsgBox(data(iCol))
                    End If
                Next
                iRow = iRow + 1
            Loop
            objReader.Close()
            Button_SFFormCancel_Click(Nothing, Nothing)
        End If

    End Sub

    Private Sub C1FlexGrid_DataCollector_BeforeEdit(ByVal sender As System.Object, ByVal e As C1.Win.C1FlexGrid.RowColEventArgs)

    End Sub

    Private Sub C1FlexGrid_InputForm_StartEdit(ByVal sender As System.Object, ByVal e As C1.Win.C1FlexGrid.RowColEventArgs) Handles C1FlexGrid_InputForm.StartEdit
        If Not EvalError Then
            currentValue = C1FlexGrid_InputForm(e.Row, e.Col).ToString()
        End If
    End Sub

    Private Sub chkNotif_CheckStateChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chkNotif.CheckStateChanged
        If (chkNotif.Checked = False) Then
            myset.ListItems.Set(chkNotif.Name, "0")
        Else
            myset.ListItems.Set(chkNotif.Name, "1")
        End If

        myset.SaveXmlCurrentConfiguration()
    End Sub

    Private Sub chkMissing_CheckStateChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chkMissing.CheckStateChanged
        If chkMissing.Checked Then
            translateControl(chkMissing, "Str_YES", "YES")
            txtMissing.Enabled = True
            myset.ListItems.Set(chkMissing.Name, "1")
        Else
            translateControl(chkMissing, "Str_NO", "NO")
            txtMissing.Enabled = False
            txtMissing.Text = ""
            myset.ListItems.Set(chkMissing.Name, "0")
        End If
        myset.SaveXmlCurrentConfiguration()
    End Sub
    Private Sub txtMissing_LostFocus(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtMissing.LostFocus
        myset.ListItems.Set(txtMissing.Name, txtMissing.Text.Trim)
        myset.SaveXmlCurrentConfiguration()
    End Sub
End Class