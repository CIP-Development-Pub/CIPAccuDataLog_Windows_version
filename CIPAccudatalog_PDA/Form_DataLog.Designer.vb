<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Public Class Form_DataLog
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer
    Private mainMenu1 As System.Windows.Forms.MainMenu

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(Form_DataLog))
        Me.mainMenu1 = New System.Windows.Forms.MainMenu
        Me.MenuItemClose = New System.Windows.Forms.MenuItem
        Me.Panel_Message = New System.Windows.Forms.Panel
        Me.Button_Close = New System.Windows.Forms.Button
        Me.Label_Message = New System.Windows.Forms.Label
        Me.OpenFileDialog1 = New System.Windows.Forms.OpenFileDialog
        Me.TabPage_Options = New System.Windows.Forms.TabPage
        Me.Label_About = New System.Windows.Forms.Label
        Me.PanelOthersOptions = New System.Windows.Forms.Panel
        Me.CheckBox_SearchForSavedFiles = New System.Windows.Forms.CheckBox
        Me.Label_SearchForSavedFiles = New System.Windows.Forms.Label
        Me.ComboBox_Language = New System.Windows.Forms.ComboBox
        Me.Label_OthersOptions = New System.Windows.Forms.Label
        Me.Label_Languages = New System.Windows.Forms.Label
        Me.PanelCollectingOptions = New System.Windows.Forms.Panel
        Me.CheckBox_Autosave = New System.Windows.Forms.CheckBox
        Me.Label_Autosave = New System.Windows.Forms.Label
        Me.CheckBox_ReplaceBlankValue = New System.Windows.Forms.CheckBox
        Me.Label_ReplaceBlankValue = New System.Windows.Forms.Label
        Me.TextBox_BlankValue = New System.Windows.Forms.TextBox
        Me.Label_BlankValue = New System.Windows.Forms.Label
        Me.NumericUpDownFrozenRowsNumberFom = New System.Windows.Forms.NumericUpDown
        Me.Label_FrozenFormRowsNumber = New System.Windows.Forms.Label
        Me.NumericUpDownFrozenColsNumber = New System.Windows.Forms.NumericUpDown
        Me.CheckBox_InputForm = New System.Windows.Forms.CheckBox
        Me.Label_FrozenGridColsNumber = New System.Windows.Forms.Label
        Me.Label_CollectingOptions = New System.Windows.Forms.Label
        Me.Label_InputForm = New System.Windows.Forms.Label
        Me.PanelScanningOptions = New System.Windows.Forms.Panel
        Me.TextBoxCharConcat = New System.Windows.Forms.TextBox
        Me.ListBoxToCompare = New System.Windows.Forms.ListBox
        Me.Button_Del = New System.Windows.Forms.Button
        Me.Button_Add = New System.Windows.Forms.Button
        Me.ListBoxColumns = New System.Windows.Forms.ListBox
        Me.Label_CharacterConcat = New System.Windows.Forms.Label
        Me.Label_ScanOptions = New System.Windows.Forms.Label
        Me.PanelPrintingOptions = New System.Windows.Forms.Panel
        Me.NumericUpDownCantidad = New System.Windows.Forms.NumericUpDown
        Me.ComboBox_LabelType = New System.Windows.Forms.ComboBox
        Me.ComboBox_Port = New System.Windows.Forms.ComboBox
        Me.Label_NumberOfLabels = New System.Windows.Forms.Label
        Me.Label_LabelDesign = New System.Windows.Forms.Label
        Me.Label_PrintOptions = New System.Windows.Forms.Label
        Me.Label_Port = New System.Windows.Forms.Label
        Me.Label_OptionsTitle = New System.Windows.Forms.Label
        Me.TabPage_LoadTemplate = New System.Windows.Forms.TabPage
        Me.Panel_SavedFiles = New System.Windows.Forms.Panel
        Me.ListView_SavedFiles = New System.Windows.Forms.ListView
        Me.Label1 = New System.Windows.Forms.Label
        Me.Button_SFFormLoad = New System.Windows.Forms.Button
        Me.Button_SFFormCancel = New System.Windows.Forms.Button
        Me.Label_IITCR = New System.Windows.Forms.Label
        Me.Panel_LoadTemplate = New System.Windows.Forms.Panel
        Me.TextBoxFileName = New System.Windows.Forms.TextBox
        Me.Label_Filename = New System.Windows.Forms.Label
        Me.Label_CrossingPlan = New System.Windows.Forms.Label
        Me.ComboBox_FieldBook = New System.Windows.Forms.ComboBox
        Me.Button_SearchTemplate = New System.Windows.Forms.Button
        Me.Label_AppName = New System.Windows.Forms.Label
        Me.Label_CIP = New System.Windows.Forms.Label
        Me.PictureBox1 = New System.Windows.Forms.PictureBox
        Me.TabControl = New System.Windows.Forms.TabControl
        Me.TabPage_CollectData = New System.Windows.Forms.TabPage
        Me.Panel_InputForm = New System.Windows.Forms.Panel
        Me.Button_InputFormOK = New System.Windows.Forms.Button
        Me.Button_InputFormCancel = New System.Windows.Forms.Button
        Me.C1FlexGrid_InputForm = New C1.Win.C1FlexGrid.C1FlexGrid
        Me.C1FlexGrid_DataCollector = New C1.Win.C1FlexGrid.C1FlexGrid
        Me.PanelInfoExtra = New System.Windows.Forms.Panel
        Me.LabelGenotypeName = New System.Windows.Forms.Label
        Me.LabelRep = New System.Windows.Forms.Label
        Me.Label_GenotypeName = New System.Windows.Forms.Label
        Me.Label_Rep = New System.Windows.Forms.Label
        Me.Button_Search = New System.Windows.Forms.Button
        Me.Button_Save = New System.Windows.Forms.Button
        Me.TextBox_barcode = New System.Windows.Forms.TextBox
        Me.Button_Print = New System.Windows.Forms.Button
        Me.Label_CollectDataTitle = New System.Windows.Forms.Label
        Me.ContextMenu1 = New System.Windows.Forms.ContextMenu
        Me.MenuItemCopy = New System.Windows.Forms.MenuItem
        Me.MenuItemPaste = New System.Windows.Forms.MenuItem
        Me.Label2 = New System.Windows.Forms.Label
        Me.chkNotif = New System.Windows.Forms.CheckBox
        Me.Label3 = New System.Windows.Forms.Label
        Me.chkMissing = New System.Windows.Forms.CheckBox
        Me.Label4 = New System.Windows.Forms.Label
        Me.txtMissing = New System.Windows.Forms.TextBox
        Me.Label5 = New System.Windows.Forms.Label
        Me.Panel_Message.SuspendLayout()
        Me.TabPage_Options.SuspendLayout()
        Me.PanelOthersOptions.SuspendLayout()
        Me.PanelCollectingOptions.SuspendLayout()
        Me.PanelScanningOptions.SuspendLayout()
        Me.PanelPrintingOptions.SuspendLayout()
        Me.TabPage_LoadTemplate.SuspendLayout()
        Me.Panel_SavedFiles.SuspendLayout()
        Me.Panel_LoadTemplate.SuspendLayout()
        Me.TabControl.SuspendLayout()
        Me.TabPage_CollectData.SuspendLayout()
        Me.Panel_InputForm.SuspendLayout()
        Me.PanelInfoExtra.SuspendLayout()
        Me.SuspendLayout()
        '
        'mainMenu1
        '
        Me.mainMenu1.MenuItems.Add(Me.MenuItemClose)
        '
        'MenuItemClose
        '
        Me.MenuItemClose.Text = "Close"
        '
        'Panel_Message
        '
        Me.Panel_Message.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.Panel_Message.Controls.Add(Me.Button_Close)
        Me.Panel_Message.Controls.Add(Me.Label_Message)
        Me.Panel_Message.Location = New System.Drawing.Point(215, 243)
        Me.Panel_Message.Name = "Panel_Message"
        Me.Panel_Message.Size = New System.Drawing.Size(25, 25)
        Me.Panel_Message.Visible = False
        '
        'Button_Close
        '
        Me.Button_Close.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(89, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.Button_Close.ForeColor = System.Drawing.Color.White
        Me.Button_Close.Location = New System.Drawing.Point(84, 230)
        Me.Button_Close.Name = "Button_Close"
        Me.Button_Close.Size = New System.Drawing.Size(72, 20)
        Me.Button_Close.TabIndex = 1
        Me.Button_Close.Text = "Close"
        '
        'Label_Message
        '
        Me.Label_Message.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.Label_Message.Font = New System.Drawing.Font("Tahoma", 9.0!, System.Drawing.FontStyle.Bold)
        Me.Label_Message.ForeColor = System.Drawing.Color.Red
        Me.Label_Message.Location = New System.Drawing.Point(8, 4)
        Me.Label_Message.Name = "Label_Message"
        Me.Label_Message.Size = New System.Drawing.Size(227, 230)
        '
        'OpenFileDialog1
        '
        Me.OpenFileDialog1.FileName = "OpenFileDialog1"
        '
        'TabPage_Options
        '
        Me.TabPage_Options.AutoScroll = True
        Me.TabPage_Options.BackColor = System.Drawing.Color.White
        Me.TabPage_Options.Controls.Add(Me.Label_About)
        Me.TabPage_Options.Controls.Add(Me.PanelOthersOptions)
        Me.TabPage_Options.Controls.Add(Me.PanelCollectingOptions)
        Me.TabPage_Options.Controls.Add(Me.PanelScanningOptions)
        Me.TabPage_Options.Controls.Add(Me.PanelPrintingOptions)
        Me.TabPage_Options.Controls.Add(Me.Label_OptionsTitle)
        Me.TabPage_Options.Location = New System.Drawing.Point(0, 0)
        Me.TabPage_Options.Name = "TabPage_Options"
        Me.TabPage_Options.Size = New System.Drawing.Size(240, 243)
        Me.TabPage_Options.Text = "Options"
        '
        'Label_About
        '
        Me.Label_About.Dock = System.Windows.Forms.DockStyle.Top
        Me.Label_About.Font = New System.Drawing.Font("Tahoma", 6.0!, System.Drawing.FontStyle.Regular)
        Me.Label_About.Location = New System.Drawing.Point(0, 644)
        Me.Label_About.Name = "Label_About"
        Me.Label_About.Size = New System.Drawing.Size(227, 47)
        Me.Label_About.Text = "About CIP ACCUDATALOG SYSTEM" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "Version: 1.2.0" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "Integrated IT & Computational Resea" & _
            "rch" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "� 2014 International Potato Center"
        Me.Label_About.TextAlign = System.Drawing.ContentAlignment.TopCenter
        '
        'PanelOthersOptions
        '
        Me.PanelOthersOptions.BackColor = System.Drawing.Color.FromArgb(CType(CType(241, Byte), Integer), CType(CType(241, Byte), Integer), CType(CType(241, Byte), Integer))
        Me.PanelOthersOptions.Controls.Add(Me.chkMissing)
        Me.PanelOthersOptions.Controls.Add(Me.Label4)
        Me.PanelOthersOptions.Controls.Add(Me.txtMissing)
        Me.PanelOthersOptions.Controls.Add(Me.Label5)
        Me.PanelOthersOptions.Controls.Add(Me.Label3)
        Me.PanelOthersOptions.Controls.Add(Me.chkNotif)
        Me.PanelOthersOptions.Controls.Add(Me.Label2)
        Me.PanelOthersOptions.Controls.Add(Me.CheckBox_SearchForSavedFiles)
        Me.PanelOthersOptions.Controls.Add(Me.Label_SearchForSavedFiles)
        Me.PanelOthersOptions.Controls.Add(Me.ComboBox_Language)
        Me.PanelOthersOptions.Controls.Add(Me.Label_OthersOptions)
        Me.PanelOthersOptions.Controls.Add(Me.Label_Languages)
        Me.PanelOthersOptions.Dock = System.Windows.Forms.DockStyle.Top
        Me.PanelOthersOptions.Location = New System.Drawing.Point(0, 470)
        Me.PanelOthersOptions.Name = "PanelOthersOptions"
        Me.PanelOthersOptions.Size = New System.Drawing.Size(227, 174)
        '
        'CheckBox_SearchForSavedFiles
        '
        Me.CheckBox_SearchForSavedFiles.Checked = True
        Me.CheckBox_SearchForSavedFiles.CheckState = System.Windows.Forms.CheckState.Checked
        Me.CheckBox_SearchForSavedFiles.Location = New System.Drawing.Point(160, 52)
        Me.CheckBox_SearchForSavedFiles.Name = "CheckBox_SearchForSavedFiles"
        Me.CheckBox_SearchForSavedFiles.Size = New System.Drawing.Size(50, 20)
        Me.CheckBox_SearchForSavedFiles.TabIndex = 12
        '
        'Label_SearchForSavedFiles
        '
        Me.Label_SearchForSavedFiles.ForeColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(89, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.Label_SearchForSavedFiles.Location = New System.Drawing.Point(5, 51)
        Me.Label_SearchForSavedFiles.Name = "Label_SearchForSavedFiles"
        Me.Label_SearchForSavedFiles.Size = New System.Drawing.Size(150, 20)
        Me.Label_SearchForSavedFiles.Text = "Search for saved files"
        '
        'ComboBox_Language
        '
        Me.ComboBox_Language.Location = New System.Drawing.Point(126, 24)
        Me.ComboBox_Language.Name = "ComboBox_Language"
        Me.ComboBox_Language.Size = New System.Drawing.Size(92, 22)
        Me.ComboBox_Language.TabIndex = 10
        '
        'Label_OthersOptions
        '
        Me.Label_OthersOptions.BackColor = System.Drawing.Color.DimGray
        Me.Label_OthersOptions.Dock = System.Windows.Forms.DockStyle.Top
        Me.Label_OthersOptions.Font = New System.Drawing.Font("Tahoma", 9.0!, System.Drawing.FontStyle.Bold)
        Me.Label_OthersOptions.ForeColor = System.Drawing.Color.White
        Me.Label_OthersOptions.Location = New System.Drawing.Point(0, 0)
        Me.Label_OthersOptions.Name = "Label_OthersOptions"
        Me.Label_OthersOptions.Size = New System.Drawing.Size(227, 19)
        Me.Label_OthersOptions.Text = "Other Options"
        '
        'Label_Languages
        '
        Me.Label_Languages.ForeColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(89, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.Label_Languages.Location = New System.Drawing.Point(5, 25)
        Me.Label_Languages.Name = "Label_Languages"
        Me.Label_Languages.Size = New System.Drawing.Size(100, 20)
        Me.Label_Languages.Text = "Language"
        '
        'PanelCollectingOptions
        '
        Me.PanelCollectingOptions.BackColor = System.Drawing.Color.FromArgb(CType(CType(241, Byte), Integer), CType(CType(241, Byte), Integer), CType(CType(241, Byte), Integer))
        Me.PanelCollectingOptions.Controls.Add(Me.CheckBox_Autosave)
        Me.PanelCollectingOptions.Controls.Add(Me.Label_Autosave)
        Me.PanelCollectingOptions.Controls.Add(Me.CheckBox_ReplaceBlankValue)
        Me.PanelCollectingOptions.Controls.Add(Me.Label_ReplaceBlankValue)
        Me.PanelCollectingOptions.Controls.Add(Me.TextBox_BlankValue)
        Me.PanelCollectingOptions.Controls.Add(Me.Label_BlankValue)
        Me.PanelCollectingOptions.Controls.Add(Me.NumericUpDownFrozenRowsNumberFom)
        Me.PanelCollectingOptions.Controls.Add(Me.Label_FrozenFormRowsNumber)
        Me.PanelCollectingOptions.Controls.Add(Me.NumericUpDownFrozenColsNumber)
        Me.PanelCollectingOptions.Controls.Add(Me.CheckBox_InputForm)
        Me.PanelCollectingOptions.Controls.Add(Me.Label_FrozenGridColsNumber)
        Me.PanelCollectingOptions.Controls.Add(Me.Label_CollectingOptions)
        Me.PanelCollectingOptions.Controls.Add(Me.Label_InputForm)
        Me.PanelCollectingOptions.Dock = System.Windows.Forms.DockStyle.Top
        Me.PanelCollectingOptions.Location = New System.Drawing.Point(0, 293)
        Me.PanelCollectingOptions.Name = "PanelCollectingOptions"
        Me.PanelCollectingOptions.Size = New System.Drawing.Size(227, 177)
        '
        'CheckBox_Autosave
        '
        Me.CheckBox_Autosave.Checked = True
        Me.CheckBox_Autosave.CheckState = System.Windows.Forms.CheckState.Checked
        Me.CheckBox_Autosave.Location = New System.Drawing.Point(160, 147)
        Me.CheckBox_Autosave.Name = "CheckBox_Autosave"
        Me.CheckBox_Autosave.Size = New System.Drawing.Size(50, 20)
        Me.CheckBox_Autosave.TabIndex = 9
        '
        'Label_Autosave
        '
        Me.Label_Autosave.ForeColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(89, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.Label_Autosave.Location = New System.Drawing.Point(5, 146)
        Me.Label_Autosave.Name = "Label_Autosave"
        Me.Label_Autosave.Size = New System.Drawing.Size(150, 20)
        Me.Label_Autosave.Text = "Autosave"
        '
        'CheckBox_ReplaceBlankValue
        '
        Me.CheckBox_ReplaceBlankValue.Location = New System.Drawing.Point(160, 99)
        Me.CheckBox_ReplaceBlankValue.Name = "CheckBox_ReplaceBlankValue"
        Me.CheckBox_ReplaceBlankValue.Size = New System.Drawing.Size(50, 20)
        Me.CheckBox_ReplaceBlankValue.TabIndex = 7
        Me.CheckBox_ReplaceBlankValue.Text = "NO"
        '
        'Label_ReplaceBlankValue
        '
        Me.Label_ReplaceBlankValue.ForeColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(89, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.Label_ReplaceBlankValue.Location = New System.Drawing.Point(5, 101)
        Me.Label_ReplaceBlankValue.Name = "Label_ReplaceBlankValue"
        Me.Label_ReplaceBlankValue.Size = New System.Drawing.Size(150, 20)
        Me.Label_ReplaceBlankValue.Text = "Replace empty cells"
        '
        'TextBox_BlankValue
        '
        Me.TextBox_BlankValue.Enabled = False
        Me.TextBox_BlankValue.Location = New System.Drawing.Point(161, 122)
        Me.TextBox_BlankValue.MaxLength = 3
        Me.TextBox_BlankValue.Name = "TextBox_BlankValue"
        Me.TextBox_BlankValue.Size = New System.Drawing.Size(55, 21)
        Me.TextBox_BlankValue.TabIndex = 8
        '
        'Label_BlankValue
        '
        Me.Label_BlankValue.ForeColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(89, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.Label_BlankValue.Location = New System.Drawing.Point(5, 124)
        Me.Label_BlankValue.Name = "Label_BlankValue"
        Me.Label_BlankValue.Size = New System.Drawing.Size(150, 20)
        Me.Label_BlankValue.Text = "Empty cell value"
        '
        'NumericUpDownFrozenRowsNumberFom
        '
        Me.NumericUpDownFrozenRowsNumberFom.Enabled = False
        Me.NumericUpDownFrozenRowsNumberFom.Location = New System.Drawing.Point(161, 74)
        Me.NumericUpDownFrozenRowsNumberFom.Name = "NumericUpDownFrozenRowsNumberFom"
        Me.NumericUpDownFrozenRowsNumberFom.Size = New System.Drawing.Size(55, 22)
        Me.NumericUpDownFrozenRowsNumberFom.TabIndex = 6
        Me.NumericUpDownFrozenRowsNumberFom.Value = New Decimal(New Integer() {1, 0, 0, 0})
        '
        'Label_FrozenFormRowsNumber
        '
        Me.Label_FrozenFormRowsNumber.ForeColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(89, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.Label_FrozenFormRowsNumber.Location = New System.Drawing.Point(5, 77)
        Me.Label_FrozenFormRowsNumber.Name = "Label_FrozenFormRowsNumber"
        Me.Label_FrozenFormRowsNumber.Size = New System.Drawing.Size(150, 20)
        Me.Label_FrozenFormRowsNumber.Text = "No of Frozen Form Rows"
        '
        'NumericUpDownFrozenColsNumber
        '
        Me.NumericUpDownFrozenColsNumber.Location = New System.Drawing.Point(161, 23)
        Me.NumericUpDownFrozenColsNumber.Name = "NumericUpDownFrozenColsNumber"
        Me.NumericUpDownFrozenColsNumber.Size = New System.Drawing.Size(55, 22)
        Me.NumericUpDownFrozenColsNumber.TabIndex = 4
        Me.NumericUpDownFrozenColsNumber.Value = New Decimal(New Integer() {1, 0, 0, 0})
        '
        'CheckBox_InputForm
        '
        Me.CheckBox_InputForm.Location = New System.Drawing.Point(161, 50)
        Me.CheckBox_InputForm.Name = "CheckBox_InputForm"
        Me.CheckBox_InputForm.Size = New System.Drawing.Size(50, 20)
        Me.CheckBox_InputForm.TabIndex = 5
        Me.CheckBox_InputForm.Text = "NO"
        '
        'Label_FrozenGridColsNumber
        '
        Me.Label_FrozenGridColsNumber.ForeColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(89, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.Label_FrozenGridColsNumber.Location = New System.Drawing.Point(5, 26)
        Me.Label_FrozenGridColsNumber.Name = "Label_FrozenGridColsNumber"
        Me.Label_FrozenGridColsNumber.Size = New System.Drawing.Size(150, 20)
        Me.Label_FrozenGridColsNumber.Text = "No of Frozen Grid Cols"
        '
        'Label_CollectingOptions
        '
        Me.Label_CollectingOptions.BackColor = System.Drawing.Color.DimGray
        Me.Label_CollectingOptions.Dock = System.Windows.Forms.DockStyle.Top
        Me.Label_CollectingOptions.Font = New System.Drawing.Font("Tahoma", 9.0!, System.Drawing.FontStyle.Bold)
        Me.Label_CollectingOptions.ForeColor = System.Drawing.Color.White
        Me.Label_CollectingOptions.Location = New System.Drawing.Point(0, 0)
        Me.Label_CollectingOptions.Name = "Label_CollectingOptions"
        Me.Label_CollectingOptions.Size = New System.Drawing.Size(227, 19)
        Me.Label_CollectingOptions.Text = "Collecting options"
        '
        'Label_InputForm
        '
        Me.Label_InputForm.ForeColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(89, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.Label_InputForm.Location = New System.Drawing.Point(5, 52)
        Me.Label_InputForm.Name = "Label_InputForm"
        Me.Label_InputForm.Size = New System.Drawing.Size(150, 20)
        Me.Label_InputForm.Text = "Use Input Form"
        '
        'PanelScanningOptions
        '
        Me.PanelScanningOptions.BackColor = System.Drawing.Color.FromArgb(CType(CType(241, Byte), Integer), CType(CType(241, Byte), Integer), CType(CType(241, Byte), Integer))
        Me.PanelScanningOptions.Controls.Add(Me.TextBoxCharConcat)
        Me.PanelScanningOptions.Controls.Add(Me.ListBoxToCompare)
        Me.PanelScanningOptions.Controls.Add(Me.Button_Del)
        Me.PanelScanningOptions.Controls.Add(Me.Button_Add)
        Me.PanelScanningOptions.Controls.Add(Me.ListBoxColumns)
        Me.PanelScanningOptions.Controls.Add(Me.Label_CharacterConcat)
        Me.PanelScanningOptions.Controls.Add(Me.Label_ScanOptions)
        Me.PanelScanningOptions.Dock = System.Windows.Forms.DockStyle.Top
        Me.PanelScanningOptions.Location = New System.Drawing.Point(0, 120)
        Me.PanelScanningOptions.Name = "PanelScanningOptions"
        Me.PanelScanningOptions.Size = New System.Drawing.Size(227, 173)
        '
        'TextBoxCharConcat
        '
        Me.TextBoxCharConcat.Location = New System.Drawing.Point(167, 23)
        Me.TextBoxCharConcat.Name = "TextBoxCharConcat"
        Me.TextBoxCharConcat.Size = New System.Drawing.Size(51, 21)
        Me.TextBoxCharConcat.TabIndex = 8
        '
        'ListBoxToCompare
        '
        Me.ListBoxToCompare.Location = New System.Drawing.Point(118, 77)
        Me.ListBoxToCompare.Name = "ListBoxToCompare"
        Me.ListBoxToCompare.Size = New System.Drawing.Size(100, 86)
        Me.ListBoxToCompare.TabIndex = 10
        '
        'Button_Del
        '
        Me.Button_Del.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(89, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.Button_Del.ForeColor = System.Drawing.Color.White
        Me.Button_Del.Location = New System.Drawing.Point(118, 51)
        Me.Button_Del.Name = "Button_Del"
        Me.Button_Del.Size = New System.Drawing.Size(30, 20)
        Me.Button_Del.TabIndex = 10
        Me.Button_Del.Text = "<"
        '
        'Button_Add
        '
        Me.Button_Add.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(89, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.Button_Add.ForeColor = System.Drawing.Color.White
        Me.Button_Add.Location = New System.Drawing.Point(80, 51)
        Me.Button_Add.Name = "Button_Add"
        Me.Button_Add.Size = New System.Drawing.Size(30, 20)
        Me.Button_Add.TabIndex = 9
        Me.Button_Add.Text = ">"
        '
        'ListBoxColumns
        '
        Me.ListBoxColumns.Location = New System.Drawing.Point(10, 77)
        Me.ListBoxColumns.Name = "ListBoxColumns"
        Me.ListBoxColumns.Size = New System.Drawing.Size(100, 86)
        Me.ListBoxColumns.TabIndex = 9
        '
        'Label_CharacterConcat
        '
        Me.Label_CharacterConcat.ForeColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(89, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.Label_CharacterConcat.Location = New System.Drawing.Point(5, 24)
        Me.Label_CharacterConcat.Name = "Label_CharacterConcat"
        Me.Label_CharacterConcat.Size = New System.Drawing.Size(150, 20)
        Me.Label_CharacterConcat.Text = "Character concat"
        '
        'Label_ScanOptions
        '
        Me.Label_ScanOptions.BackColor = System.Drawing.Color.DimGray
        Me.Label_ScanOptions.Dock = System.Windows.Forms.DockStyle.Top
        Me.Label_ScanOptions.Font = New System.Drawing.Font("Tahoma", 9.0!, System.Drawing.FontStyle.Bold)
        Me.Label_ScanOptions.ForeColor = System.Drawing.Color.White
        Me.Label_ScanOptions.Location = New System.Drawing.Point(0, 0)
        Me.Label_ScanOptions.Name = "Label_ScanOptions"
        Me.Label_ScanOptions.Size = New System.Drawing.Size(227, 19)
        Me.Label_ScanOptions.Text = "Scan options"
        '
        'PanelPrintingOptions
        '
        Me.PanelPrintingOptions.BackColor = System.Drawing.Color.FromArgb(CType(CType(241, Byte), Integer), CType(CType(241, Byte), Integer), CType(CType(241, Byte), Integer))
        Me.PanelPrintingOptions.Controls.Add(Me.NumericUpDownCantidad)
        Me.PanelPrintingOptions.Controls.Add(Me.ComboBox_LabelType)
        Me.PanelPrintingOptions.Controls.Add(Me.ComboBox_Port)
        Me.PanelPrintingOptions.Controls.Add(Me.Label_NumberOfLabels)
        Me.PanelPrintingOptions.Controls.Add(Me.Label_LabelDesign)
        Me.PanelPrintingOptions.Controls.Add(Me.Label_PrintOptions)
        Me.PanelPrintingOptions.Controls.Add(Me.Label_Port)
        Me.PanelPrintingOptions.Dock = System.Windows.Forms.DockStyle.Top
        Me.PanelPrintingOptions.Location = New System.Drawing.Point(0, 20)
        Me.PanelPrintingOptions.Name = "PanelPrintingOptions"
        Me.PanelPrintingOptions.Size = New System.Drawing.Size(227, 100)
        '
        'NumericUpDownCantidad
        '
        Me.NumericUpDownCantidad.Location = New System.Drawing.Point(162, 70)
        Me.NumericUpDownCantidad.Minimum = New Decimal(New Integer() {1, 0, 0, 0})
        Me.NumericUpDownCantidad.Name = "NumericUpDownCantidad"
        Me.NumericUpDownCantidad.Size = New System.Drawing.Size(55, 22)
        Me.NumericUpDownCantidad.TabIndex = 3
        Me.NumericUpDownCantidad.Value = New Decimal(New Integer() {1, 0, 0, 0})
        '
        'ComboBox_LabelType
        '
        Me.ComboBox_LabelType.Items.Add("Clone Selector")
        Me.ComboBox_LabelType.Items.Add("Other 1")
        Me.ComboBox_LabelType.Items.Add("Other 2")
        Me.ComboBox_LabelType.Items.Add("Other 3")
        Me.ComboBox_LabelType.Location = New System.Drawing.Point(126, 45)
        Me.ComboBox_LabelType.Name = "ComboBox_LabelType"
        Me.ComboBox_LabelType.Size = New System.Drawing.Size(92, 22)
        Me.ComboBox_LabelType.TabIndex = 2
        '
        'ComboBox_Port
        '
        Me.ComboBox_Port.Items.Add("COM0:")
        Me.ComboBox_Port.Items.Add("COM1:")
        Me.ComboBox_Port.Items.Add("COM2:")
        Me.ComboBox_Port.Items.Add("COM3:")
        Me.ComboBox_Port.Items.Add("COM4:")
        Me.ComboBox_Port.Items.Add("COM5:")
        Me.ComboBox_Port.Items.Add("COM6:")
        Me.ComboBox_Port.Items.Add("COM7:")
        Me.ComboBox_Port.Items.Add("COM8:")
        Me.ComboBox_Port.Items.Add("COM9:")
        Me.ComboBox_Port.Location = New System.Drawing.Point(126, 20)
        Me.ComboBox_Port.Name = "ComboBox_Port"
        Me.ComboBox_Port.Size = New System.Drawing.Size(92, 22)
        Me.ComboBox_Port.TabIndex = 1
        '
        'Label_NumberOfLabels
        '
        Me.Label_NumberOfLabels.ForeColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(89, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.Label_NumberOfLabels.Location = New System.Drawing.Point(5, 71)
        Me.Label_NumberOfLabels.Name = "Label_NumberOfLabels"
        Me.Label_NumberOfLabels.Size = New System.Drawing.Size(125, 20)
        Me.Label_NumberOfLabels.Text = "No of labels"
        '
        'Label_LabelDesign
        '
        Me.Label_LabelDesign.ForeColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(89, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.Label_LabelDesign.Location = New System.Drawing.Point(5, 47)
        Me.Label_LabelDesign.Name = "Label_LabelDesign"
        Me.Label_LabelDesign.Size = New System.Drawing.Size(115, 20)
        Me.Label_LabelDesign.Text = "Label Design"
        '
        'Label_PrintOptions
        '
        Me.Label_PrintOptions.BackColor = System.Drawing.Color.DimGray
        Me.Label_PrintOptions.Dock = System.Windows.Forms.DockStyle.Top
        Me.Label_PrintOptions.Font = New System.Drawing.Font("Tahoma", 9.0!, System.Drawing.FontStyle.Bold)
        Me.Label_PrintOptions.ForeColor = System.Drawing.Color.White
        Me.Label_PrintOptions.Location = New System.Drawing.Point(0, 0)
        Me.Label_PrintOptions.Name = "Label_PrintOptions"
        Me.Label_PrintOptions.Size = New System.Drawing.Size(227, 19)
        Me.Label_PrintOptions.Text = "Printing options"
        '
        'Label_Port
        '
        Me.Label_Port.ForeColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(89, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.Label_Port.Location = New System.Drawing.Point(5, 23)
        Me.Label_Port.Name = "Label_Port"
        Me.Label_Port.Size = New System.Drawing.Size(100, 20)
        Me.Label_Port.Text = "Port"
        '
        'Label_OptionsTitle
        '
        Me.Label_OptionsTitle.BackColor = System.Drawing.Color.White
        Me.Label_OptionsTitle.Dock = System.Windows.Forms.DockStyle.Top
        Me.Label_OptionsTitle.Font = New System.Drawing.Font("Tahoma", 10.0!, System.Drawing.FontStyle.Bold)
        Me.Label_OptionsTitle.ForeColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(89, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.Label_OptionsTitle.Location = New System.Drawing.Point(0, 0)
        Me.Label_OptionsTitle.Name = "Label_OptionsTitle"
        Me.Label_OptionsTitle.Size = New System.Drawing.Size(227, 20)
        Me.Label_OptionsTitle.Text = "Options"
        Me.Label_OptionsTitle.TextAlign = System.Drawing.ContentAlignment.TopCenter
        '
        'TabPage_LoadTemplate
        '
        Me.TabPage_LoadTemplate.AutoScroll = True
        Me.TabPage_LoadTemplate.Controls.Add(Me.Panel_SavedFiles)
        Me.TabPage_LoadTemplate.Controls.Add(Me.Label_IITCR)
        Me.TabPage_LoadTemplate.Controls.Add(Me.Panel_LoadTemplate)
        Me.TabPage_LoadTemplate.Controls.Add(Me.Label_AppName)
        Me.TabPage_LoadTemplate.Controls.Add(Me.Label_CIP)
        Me.TabPage_LoadTemplate.Controls.Add(Me.PictureBox1)
        Me.TabPage_LoadTemplate.Location = New System.Drawing.Point(0, 0)
        Me.TabPage_LoadTemplate.Name = "TabPage_LoadTemplate"
        Me.TabPage_LoadTemplate.Size = New System.Drawing.Size(240, 243)
        Me.TabPage_LoadTemplate.Text = "Load Template"
        '
        'Panel_SavedFiles
        '
        Me.Panel_SavedFiles.Controls.Add(Me.ListView_SavedFiles)
        Me.Panel_SavedFiles.Controls.Add(Me.Label1)
        Me.Panel_SavedFiles.Controls.Add(Me.Button_SFFormLoad)
        Me.Panel_SavedFiles.Controls.Add(Me.Button_SFFormCancel)
        Me.Panel_SavedFiles.Location = New System.Drawing.Point(240, 0)
        Me.Panel_SavedFiles.Name = "Panel_SavedFiles"
        Me.Panel_SavedFiles.Size = New System.Drawing.Size(240, 243)
        Me.Panel_SavedFiles.Visible = False
        '
        'ListView_SavedFiles
        '
        Me.ListView_SavedFiles.Dock = System.Windows.Forms.DockStyle.Top
        Me.ListView_SavedFiles.Location = New System.Drawing.Point(0, 20)
        Me.ListView_SavedFiles.Name = "ListView_SavedFiles"
        Me.ListView_SavedFiles.Size = New System.Drawing.Size(240, 182)
        Me.ListView_SavedFiles.TabIndex = 12
        Me.ListView_SavedFiles.View = System.Windows.Forms.View.List
        '
        'Label1
        '
        Me.Label1.Dock = System.Windows.Forms.DockStyle.Top
        Me.Label1.Location = New System.Drawing.Point(0, 0)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(240, 20)
        Me.Label1.Text = "There are posible saved files"
        '
        'Button_SFFormLoad
        '
        Me.Button_SFFormLoad.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(89, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.Button_SFFormLoad.ForeColor = System.Drawing.Color.White
        Me.Button_SFFormLoad.Location = New System.Drawing.Point(10, 208)
        Me.Button_SFFormLoad.Name = "Button_SFFormLoad"
        Me.Button_SFFormLoad.Size = New System.Drawing.Size(105, 21)
        Me.Button_SFFormLoad.TabIndex = 10
        Me.Button_SFFormLoad.Text = "Load"
        '
        'Button_SFFormCancel
        '
        Me.Button_SFFormCancel.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(89, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.Button_SFFormCancel.ForeColor = System.Drawing.Color.White
        Me.Button_SFFormCancel.Location = New System.Drawing.Point(123, 208)
        Me.Button_SFFormCancel.Name = "Button_SFFormCancel"
        Me.Button_SFFormCancel.Size = New System.Drawing.Size(105, 21)
        Me.Button_SFFormCancel.TabIndex = 9
        Me.Button_SFFormCancel.Text = "Cancel"
        '
        'Label_IITCR
        '
        Me.Label_IITCR.Font = New System.Drawing.Font("Tahoma", 6.0!, System.Drawing.FontStyle.Regular)
        Me.Label_IITCR.Location = New System.Drawing.Point(4, 233)
        Me.Label_IITCR.Name = "Label_IITCR"
        Me.Label_IITCR.Size = New System.Drawing.Size(233, 11)
        Me.Label_IITCR.Text = "Integrated IT and Computational Research"
        Me.Label_IITCR.TextAlign = System.Drawing.ContentAlignment.TopCenter
        '
        'Panel_LoadTemplate
        '
        Me.Panel_LoadTemplate.BackColor = System.Drawing.Color.FromArgb(CType(CType(241, Byte), Integer), CType(CType(241, Byte), Integer), CType(CType(241, Byte), Integer))
        Me.Panel_LoadTemplate.Controls.Add(Me.TextBoxFileName)
        Me.Panel_LoadTemplate.Controls.Add(Me.Label_Filename)
        Me.Panel_LoadTemplate.Controls.Add(Me.Label_CrossingPlan)
        Me.Panel_LoadTemplate.Controls.Add(Me.ComboBox_FieldBook)
        Me.Panel_LoadTemplate.Controls.Add(Me.Button_SearchTemplate)
        Me.Panel_LoadTemplate.Location = New System.Drawing.Point(9, 82)
        Me.Panel_LoadTemplate.Name = "Panel_LoadTemplate"
        Me.Panel_LoadTemplate.Size = New System.Drawing.Size(224, 139)
        '
        'TextBoxFileName
        '
        Me.TextBoxFileName.Enabled = False
        Me.TextBoxFileName.Location = New System.Drawing.Point(10, 77)
        Me.TextBoxFileName.Name = "TextBoxFileName"
        Me.TextBoxFileName.ReadOnly = True
        Me.TextBoxFileName.Size = New System.Drawing.Size(200, 21)
        Me.TextBoxFileName.TabIndex = 6
        '
        'Label_Filename
        '
        Me.Label_Filename.ForeColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(89, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.Label_Filename.Location = New System.Drawing.Point(10, 57)
        Me.Label_Filename.Name = "Label_Filename"
        Me.Label_Filename.Size = New System.Drawing.Size(200, 16)
        Me.Label_Filename.Text = "Filename"
        '
        'Label_CrossingPlan
        '
        Me.Label_CrossingPlan.ForeColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(89, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.Label_CrossingPlan.Location = New System.Drawing.Point(10, 10)
        Me.Label_CrossingPlan.Name = "Label_CrossingPlan"
        Me.Label_CrossingPlan.Size = New System.Drawing.Size(200, 16)
        Me.Label_CrossingPlan.Text = "Fieldbook"
        '
        'ComboBox_FieldBook
        '
        Me.ComboBox_FieldBook.Items.Add("Clone Selector")
        Me.ComboBox_FieldBook.Items.Add("Custom Fieldbook")
        Me.ComboBox_FieldBook.Location = New System.Drawing.Point(10, 30)
        Me.ComboBox_FieldBook.Name = "ComboBox_FieldBook"
        Me.ComboBox_FieldBook.Size = New System.Drawing.Size(200, 22)
        Me.ComboBox_FieldBook.TabIndex = 8
        '
        'Button_SearchTemplate
        '
        Me.Button_SearchTemplate.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(89, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.Button_SearchTemplate.ForeColor = System.Drawing.Color.White
        Me.Button_SearchTemplate.Location = New System.Drawing.Point(71, 109)
        Me.Button_SearchTemplate.Name = "Button_SearchTemplate"
        Me.Button_SearchTemplate.Size = New System.Drawing.Size(80, 21)
        Me.Button_SearchTemplate.TabIndex = 7
        Me.Button_SearchTemplate.Text = "Search"
        '
        'Label_AppName
        '
        Me.Label_AppName.Font = New System.Drawing.Font("Tahoma", 10.0!, System.Drawing.FontStyle.Bold)
        Me.Label_AppName.ForeColor = System.Drawing.Color.FromArgb(CType(CType(76, Byte), Integer), CType(CType(143, Byte), Integer), CType(CType(251, Byte), Integer))
        Me.Label_AppName.Location = New System.Drawing.Point(80, 46)
        Me.Label_AppName.Name = "Label_AppName"
        Me.Label_AppName.Size = New System.Drawing.Size(150, 16)
        Me.Label_AppName.Text = "CIP ACCUDATALOG"
        Me.Label_AppName.TextAlign = System.Drawing.ContentAlignment.TopCenter
        '
        'Label_CIP
        '
        Me.Label_CIP.Font = New System.Drawing.Font("Tahoma", 10.0!, System.Drawing.FontStyle.Bold)
        Me.Label_CIP.ForeColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(89, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.Label_CIP.Location = New System.Drawing.Point(80, 9)
        Me.Label_CIP.Name = "Label_CIP"
        Me.Label_CIP.Size = New System.Drawing.Size(150, 32)
        Me.Label_CIP.Text = "International Potato Center"
        Me.Label_CIP.TextAlign = System.Drawing.ContentAlignment.TopCenter
        '
        'PictureBox1
        '
        Me.PictureBox1.BackColor = System.Drawing.Color.White
        Me.PictureBox1.Image = CType(resources.GetObject("PictureBox1.Image"), System.Drawing.Image)
        Me.PictureBox1.Location = New System.Drawing.Point(9, 9)
        Me.PictureBox1.Name = "PictureBox1"
        Me.PictureBox1.Size = New System.Drawing.Size(61, 61)
        Me.PictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        '
        'TabControl
        '
        Me.TabControl.Controls.Add(Me.TabPage_LoadTemplate)
        Me.TabControl.Controls.Add(Me.TabPage_CollectData)
        Me.TabControl.Controls.Add(Me.TabPage_Options)
        Me.TabControl.Dock = System.Windows.Forms.DockStyle.Fill
        Me.TabControl.Font = New System.Drawing.Font("Tahoma", 9.0!, System.Drawing.FontStyle.Regular)
        Me.TabControl.Location = New System.Drawing.Point(0, 0)
        Me.TabControl.Name = "TabControl"
        Me.TabControl.SelectedIndex = 0
        Me.TabControl.Size = New System.Drawing.Size(240, 268)
        Me.TabControl.TabIndex = 0
        '
        'TabPage_CollectData
        '
        Me.TabPage_CollectData.AutoScroll = True
        Me.TabPage_CollectData.BackColor = System.Drawing.Color.White
        Me.TabPage_CollectData.Controls.Add(Me.Panel_InputForm)
        Me.TabPage_CollectData.Controls.Add(Me.C1FlexGrid_DataCollector)
        Me.TabPage_CollectData.Controls.Add(Me.PanelInfoExtra)
        Me.TabPage_CollectData.Controls.Add(Me.Button_Search)
        Me.TabPage_CollectData.Controls.Add(Me.Button_Save)
        Me.TabPage_CollectData.Controls.Add(Me.TextBox_barcode)
        Me.TabPage_CollectData.Controls.Add(Me.Button_Print)
        Me.TabPage_CollectData.Controls.Add(Me.Label_CollectDataTitle)
        Me.TabPage_CollectData.Location = New System.Drawing.Point(0, 0)
        Me.TabPage_CollectData.Name = "TabPage_CollectData"
        Me.TabPage_CollectData.Size = New System.Drawing.Size(240, 243)
        Me.TabPage_CollectData.Text = "Collect Data"
        '
        'Panel_InputForm
        '
        Me.Panel_InputForm.Controls.Add(Me.Button_InputFormOK)
        Me.Panel_InputForm.Controls.Add(Me.Button_InputFormCancel)
        Me.Panel_InputForm.Controls.Add(Me.C1FlexGrid_InputForm)
        Me.Panel_InputForm.Location = New System.Drawing.Point(240, 0)
        Me.Panel_InputForm.Name = "Panel_InputForm"
        Me.Panel_InputForm.Size = New System.Drawing.Size(240, 243)
        Me.Panel_InputForm.Visible = False
        '
        'Button_InputFormOK
        '
        Me.Button_InputFormOK.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(89, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.Button_InputFormOK.ForeColor = System.Drawing.Color.White
        Me.Button_InputFormOK.Location = New System.Drawing.Point(10, 221)
        Me.Button_InputFormOK.Name = "Button_InputFormOK"
        Me.Button_InputFormOK.Size = New System.Drawing.Size(105, 21)
        Me.Button_InputFormOK.TabIndex = 8
        Me.Button_InputFormOK.Text = "OK"
        '
        'Button_InputFormCancel
        '
        Me.Button_InputFormCancel.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(89, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.Button_InputFormCancel.ForeColor = System.Drawing.Color.White
        Me.Button_InputFormCancel.Location = New System.Drawing.Point(123, 221)
        Me.Button_InputFormCancel.Name = "Button_InputFormCancel"
        Me.Button_InputFormCancel.Size = New System.Drawing.Size(105, 21)
        Me.Button_InputFormCancel.TabIndex = 7
        Me.Button_InputFormCancel.Text = "Cancel"
        '
        'C1FlexGrid_InputForm
        '
        Me.C1FlexGrid_InputForm.AllowDragging = C1.Win.C1FlexGrid.AllowDraggingEnum.None
        Me.C1FlexGrid_InputForm.AllowEditing = True
        Me.C1FlexGrid_InputForm.AllowSorting = C1.Win.C1FlexGrid.AllowSortingEnum.None
        Me.C1FlexGrid_InputForm.AutoResize = True
        Me.C1FlexGrid_InputForm.AutoSearchDelay = 1
        Me.C1FlexGrid_InputForm.BorderStyle = C1.Win.C1FlexGrid.Util.BaseControls.BorderStyleEnum.Light3D
        Me.C1FlexGrid_InputForm.Clip = ""
        Me.C1FlexGrid_InputForm.ClipSeparators = "" & Global.Microsoft.VisualBasic.ChrW(9) & Global.Microsoft.VisualBasic.ChrW(13)
        Me.C1FlexGrid_InputForm.Col = 0
        Me.C1FlexGrid_InputForm.ColSel = 0
        Me.C1FlexGrid_InputForm.ComboList = Nothing
        Me.C1FlexGrid_InputForm.EditMask = Nothing
        Me.C1FlexGrid_InputForm.ExtendLastCol = False
        Me.C1FlexGrid_InputForm.LeftCol = 1
        Me.C1FlexGrid_InputForm.Location = New System.Drawing.Point(0, 0)
        Me.C1FlexGrid_InputForm.Name = "C1FlexGrid_InputForm"
        Me.C1FlexGrid_InputForm.Redraw = True
        Me.C1FlexGrid_InputForm.Row = 0
        Me.C1FlexGrid_InputForm.RowSel = 0
        Me.C1FlexGrid_InputForm.ScrollPosition = New System.Drawing.Point(0, 0)
        Me.C1FlexGrid_InputForm.ScrollTrack = True
        Me.C1FlexGrid_InputForm.SelectionMode = C1.Win.C1FlexGrid.SelectionModeEnum.Row
        Me.C1FlexGrid_InputForm.ShowButtons = C1.Win.C1FlexGrid.ShowButtonsEnum.WhenEditing
        Me.C1FlexGrid_InputForm.ShowCursor = False
        Me.C1FlexGrid_InputForm.ShowSort = False
        Me.C1FlexGrid_InputForm.Size = New System.Drawing.Size(240, 214)
        Me.C1FlexGrid_InputForm.StyleInfo = resources.GetString("C1FlexGrid_InputForm.StyleInfo")
        Me.C1FlexGrid_InputForm.TabIndex = 0
        Me.C1FlexGrid_InputForm.TopRow = 1
        '
        'C1FlexGrid_DataCollector
        '
        Me.C1FlexGrid_DataCollector.AllowDragging = C1.Win.C1FlexGrid.AllowDraggingEnum.None
        Me.C1FlexGrid_DataCollector.AllowEditing = True
        Me.C1FlexGrid_DataCollector.AllowSorting = C1.Win.C1FlexGrid.AllowSortingEnum.None
        Me.C1FlexGrid_DataCollector.AutoResize = True
        Me.C1FlexGrid_DataCollector.AutoSearchDelay = 1
        Me.C1FlexGrid_DataCollector.BorderStyle = C1.Win.C1FlexGrid.Util.BaseControls.BorderStyleEnum.Light3D
        Me.C1FlexGrid_DataCollector.ClipSeparators = "" & Global.Microsoft.VisualBasic.ChrW(9) & Global.Microsoft.VisualBasic.ChrW(13)
        Me.C1FlexGrid_DataCollector.Col = 0
        Me.C1FlexGrid_DataCollector.ColSel = 0
        Me.C1FlexGrid_DataCollector.ComboList = Nothing
        Me.C1FlexGrid_DataCollector.EditMask = Nothing
        Me.C1FlexGrid_DataCollector.ExtendLastCol = False
        Me.C1FlexGrid_DataCollector.KeyActionEnter = C1.Win.C1FlexGrid.KeyActionEnum.None
        Me.C1FlexGrid_DataCollector.LeftCol = 1
        Me.C1FlexGrid_DataCollector.Location = New System.Drawing.Point(6, 84)
        Me.C1FlexGrid_DataCollector.Name = "C1FlexGrid_DataCollector"
        Me.C1FlexGrid_DataCollector.Redraw = True
        Me.C1FlexGrid_DataCollector.Row = -2
        Me.C1FlexGrid_DataCollector.RowSel = -2
        Me.C1FlexGrid_DataCollector.ScrollPosition = New System.Drawing.Point(0, 0)
        Me.C1FlexGrid_DataCollector.ScrollTrack = True
        Me.C1FlexGrid_DataCollector.SelectionMode = C1.Win.C1FlexGrid.SelectionModeEnum.RowRange
        Me.C1FlexGrid_DataCollector.ShowCursor = False
        Me.C1FlexGrid_DataCollector.ShowSort = False
        Me.C1FlexGrid_DataCollector.Size = New System.Drawing.Size(226, 156)
        Me.C1FlexGrid_DataCollector.StyleInfo = resources.GetString("C1FlexGrid_DataCollector.StyleInfo")
        Me.C1FlexGrid_DataCollector.TabIndex = 5
        Me.C1FlexGrid_DataCollector.TopRow = 1
        '
        'PanelInfoExtra
        '
        Me.PanelInfoExtra.BackColor = System.Drawing.Color.White
        Me.PanelInfoExtra.Controls.Add(Me.LabelGenotypeName)
        Me.PanelInfoExtra.Controls.Add(Me.LabelRep)
        Me.PanelInfoExtra.Controls.Add(Me.Label_GenotypeName)
        Me.PanelInfoExtra.Controls.Add(Me.Label_Rep)
        Me.PanelInfoExtra.Location = New System.Drawing.Point(0, 49)
        Me.PanelInfoExtra.Name = "PanelInfoExtra"
        Me.PanelInfoExtra.Size = New System.Drawing.Size(240, 35)
        '
        'LabelGenotypeName
        '
        Me.LabelGenotypeName.Font = New System.Drawing.Font("Tahoma", 8.0!, System.Drawing.FontStyle.Bold)
        Me.LabelGenotypeName.ForeColor = System.Drawing.Color.FromArgb(CType(CType(76, Byte), Integer), CType(CType(143, Byte), Integer), CType(CType(251, Byte), Integer))
        Me.LabelGenotypeName.Location = New System.Drawing.Point(6, 0)
        Me.LabelGenotypeName.Name = "LabelGenotypeName"
        Me.LabelGenotypeName.Size = New System.Drawing.Size(106, 16)
        Me.LabelGenotypeName.Text = "Genotype Name : "
        Me.LabelGenotypeName.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'LabelRep
        '
        Me.LabelRep.Font = New System.Drawing.Font("Tahoma", 8.0!, System.Drawing.FontStyle.Bold)
        Me.LabelRep.ForeColor = System.Drawing.Color.FromArgb(CType(CType(76, Byte), Integer), CType(CType(143, Byte), Integer), CType(CType(251, Byte), Integer))
        Me.LabelRep.Location = New System.Drawing.Point(6, 15)
        Me.LabelRep.Name = "LabelRep"
        Me.LabelRep.Size = New System.Drawing.Size(106, 20)
        Me.LabelRep.Text = "Rep : "
        Me.LabelRep.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'Label_GenotypeName
        '
        Me.Label_GenotypeName.Font = New System.Drawing.Font("Tahoma", 8.0!, System.Drawing.FontStyle.Bold)
        Me.Label_GenotypeName.ForeColor = System.Drawing.Color.FromArgb(CType(CType(76, Byte), Integer), CType(CType(143, Byte), Integer), CType(CType(251, Byte), Integer))
        Me.Label_GenotypeName.Location = New System.Drawing.Point(117, 0)
        Me.Label_GenotypeName.Name = "Label_GenotypeName"
        Me.Label_GenotypeName.Size = New System.Drawing.Size(115, 16)
        Me.Label_GenotypeName.Text = "------------------"
        '
        'Label_Rep
        '
        Me.Label_Rep.Font = New System.Drawing.Font("Tahoma", 8.0!, System.Drawing.FontStyle.Bold)
        Me.Label_Rep.ForeColor = System.Drawing.Color.FromArgb(CType(CType(76, Byte), Integer), CType(CType(143, Byte), Integer), CType(CType(251, Byte), Integer))
        Me.Label_Rep.Location = New System.Drawing.Point(117, 16)
        Me.Label_Rep.Name = "Label_Rep"
        Me.Label_Rep.Size = New System.Drawing.Size(115, 16)
        Me.Label_Rep.Text = "------------------"
        '
        'Button_Search
        '
        Me.Button_Search.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(89, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.Button_Search.ForeColor = System.Drawing.Color.White
        Me.Button_Search.Location = New System.Drawing.Point(172, 22)
        Me.Button_Search.Name = "Button_Search"
        Me.Button_Search.Size = New System.Drawing.Size(60, 21)
        Me.Button_Search.TabIndex = 4
        Me.Button_Search.Text = "Search"
        '
        'Button_Save
        '
        Me.Button_Save.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(89, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.Button_Save.ForeColor = System.Drawing.Color.White
        Me.Button_Save.Location = New System.Drawing.Point(4, 22)
        Me.Button_Save.Name = "Button_Save"
        Me.Button_Save.Size = New System.Drawing.Size(60, 21)
        Me.Button_Save.TabIndex = 1
        Me.Button_Save.Text = "Save"
        '
        'TextBox_barcode
        '
        Me.TextBox_barcode.Location = New System.Drawing.Point(126, 22)
        Me.TextBox_barcode.Name = "TextBox_barcode"
        Me.TextBox_barcode.Size = New System.Drawing.Size(45, 21)
        Me.TextBox_barcode.TabIndex = 3
        '
        'Button_Print
        '
        Me.Button_Print.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(89, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.Button_Print.ForeColor = System.Drawing.Color.White
        Me.Button_Print.Location = New System.Drawing.Point(65, 22)
        Me.Button_Print.Name = "Button_Print"
        Me.Button_Print.Size = New System.Drawing.Size(60, 21)
        Me.Button_Print.TabIndex = 2
        Me.Button_Print.Text = "Print"
        '
        'Label_CollectDataTitle
        '
        Me.Label_CollectDataTitle.BackColor = System.Drawing.Color.White
        Me.Label_CollectDataTitle.Font = New System.Drawing.Font("Tahoma", 10.0!, System.Drawing.FontStyle.Bold)
        Me.Label_CollectDataTitle.ForeColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(89, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.Label_CollectDataTitle.Location = New System.Drawing.Point(0, 0)
        Me.Label_CollectDataTitle.Name = "Label_CollectDataTitle"
        Me.Label_CollectDataTitle.Size = New System.Drawing.Size(240, 20)
        Me.Label_CollectDataTitle.Text = "Collect Data"
        Me.Label_CollectDataTitle.TextAlign = System.Drawing.ContentAlignment.TopCenter
        '
        'ContextMenu1
        '
        Me.ContextMenu1.MenuItems.Add(Me.MenuItemCopy)
        Me.ContextMenu1.MenuItems.Add(Me.MenuItemPaste)
        '
        'MenuItemCopy
        '
        Me.MenuItemCopy.Text = "Copy"
        '
        'MenuItemPaste
        '
        Me.MenuItemPaste.Text = "Paste"
        '
        'Label2
        '
        Me.Label2.ForeColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(89, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.Label2.Location = New System.Drawing.Point(5, 80)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(150, 20)
        Me.Label2.Text = "Notification before change"
        '
        'chkNotif
        '
        Me.chkNotif.Location = New System.Drawing.Point(160, 80)
        Me.chkNotif.Name = "chkNotif"
        Me.chkNotif.Size = New System.Drawing.Size(50, 20)
        Me.chkNotif.TabIndex = 18
        '
        'Label3
        '
        Me.Label3.ForeColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(89, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.Label3.Location = New System.Drawing.Point(5, 95)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(41, 20)
        Me.Label3.Text = "value"
        '
        'chkMissing
        '
        Me.chkMissing.Location = New System.Drawing.Point(160, 117)
        Me.chkMissing.Name = "chkMissing"
        Me.chkMissing.Size = New System.Drawing.Size(50, 20)
        Me.chkMissing.TabIndex = 23
        Me.chkMissing.Text = "NO"
        '
        'Label4
        '
        Me.Label4.ForeColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(89, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.Label4.Location = New System.Drawing.Point(5, 119)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(150, 20)
        Me.Label4.Text = "Allow missing values"
        '
        'txtMissing
        '
        Me.txtMissing.Enabled = False
        Me.txtMissing.Location = New System.Drawing.Point(161, 140)
        Me.txtMissing.MaxLength = 5
        Me.txtMissing.Name = "txtMissing"
        Me.txtMissing.Size = New System.Drawing.Size(55, 21)
        Me.txtMissing.TabIndex = 24
        '
        'Label5
        '
        Me.Label5.ForeColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(89, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.Label5.Location = New System.Drawing.Point(5, 142)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(150, 20)
        Me.Label5.Text = "Missing value"
        '
        'Form_DataLog
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(96.0!, 96.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi
        Me.AutoScroll = True
        Me.ClientSize = New System.Drawing.Size(240, 268)
        Me.Controls.Add(Me.Panel_Message)
        Me.Controls.Add(Me.TabControl)
        Me.KeyPreview = True
        Me.Menu = Me.mainMenu1
        Me.Name = "Form_DataLog"
        Me.Text = "CIP ACCUDATALOG SYSTEM"
        Me.Panel_Message.ResumeLayout(False)
        Me.TabPage_Options.ResumeLayout(False)
        Me.PanelOthersOptions.ResumeLayout(False)
        Me.PanelCollectingOptions.ResumeLayout(False)
        Me.PanelScanningOptions.ResumeLayout(False)
        Me.PanelPrintingOptions.ResumeLayout(False)
        Me.TabPage_LoadTemplate.ResumeLayout(False)
        Me.Panel_SavedFiles.ResumeLayout(False)
        Me.Panel_LoadTemplate.ResumeLayout(False)
        Me.TabControl.ResumeLayout(False)
        Me.TabPage_CollectData.ResumeLayout(False)
        Me.Panel_InputForm.ResumeLayout(False)
        Me.PanelInfoExtra.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents OpenFileDialog1 As System.Windows.Forms.OpenFileDialog
    Friend WithEvents Panel_Message As System.Windows.Forms.Panel
    Friend WithEvents Label_Message As System.Windows.Forms.Label
    Friend WithEvents Button_Close As System.Windows.Forms.Button
    Friend WithEvents TabPage_Options As System.Windows.Forms.TabPage
    Friend WithEvents PanelScanningOptions As System.Windows.Forms.Panel
    Friend WithEvents PanelPrintingOptions As System.Windows.Forms.Panel
    Friend WithEvents Label_PrintOptions As System.Windows.Forms.Label
    Friend WithEvents Label_Port As System.Windows.Forms.Label
    Friend WithEvents ComboBox_Port As System.Windows.Forms.ComboBox
    Friend WithEvents Label_OptionsTitle As System.Windows.Forms.Label
    Friend WithEvents TabPage_LoadTemplate As System.Windows.Forms.TabPage
    Friend WithEvents Panel_LoadTemplate As System.Windows.Forms.Panel
    Friend WithEvents Label_CrossingPlan As System.Windows.Forms.Label
    Friend WithEvents TextBoxFileName As System.Windows.Forms.TextBox
    Friend WithEvents Button_SearchTemplate As System.Windows.Forms.Button
    Friend WithEvents Label_AppName As System.Windows.Forms.Label
    Friend WithEvents Label_CIP As System.Windows.Forms.Label
    Friend WithEvents PictureBox1 As System.Windows.Forms.PictureBox
    Friend WithEvents TabControl As System.Windows.Forms.TabControl
    Friend WithEvents TabPage_CollectData As System.Windows.Forms.TabPage
    Friend WithEvents C1FlexGrid_DataCollector As C1.Win.C1FlexGrid.C1FlexGrid
    Friend WithEvents Label_CollectDataTitle As System.Windows.Forms.Label
    Friend WithEvents Button_Print As System.Windows.Forms.Button
    Private WithEvents ComboBox_FieldBook As System.Windows.Forms.ComboBox
    Friend WithEvents TextBox_barcode As System.Windows.Forms.TextBox
    Friend WithEvents Button_Save As System.Windows.Forms.Button
    Friend WithEvents Label_ScanOptions As System.Windows.Forms.Label
    Friend WithEvents TextBoxCharConcat As System.Windows.Forms.TextBox
    Friend WithEvents Label_CharacterConcat As System.Windows.Forms.Label
    Friend WithEvents Button_Search As System.Windows.Forms.Button
    Friend WithEvents Button_Del As System.Windows.Forms.Button
    Friend WithEvents Button_Add As System.Windows.Forms.Button
    Friend WithEvents ListBoxColumns As System.Windows.Forms.ListBox
    Friend WithEvents ListBoxToCompare As System.Windows.Forms.ListBox
    Friend WithEvents Label_IITCR As System.Windows.Forms.Label
    Friend WithEvents Label_Filename As System.Windows.Forms.Label
    Friend WithEvents LabelRep As System.Windows.Forms.Label
    Friend WithEvents LabelGenotypeName As System.Windows.Forms.Label
    Friend WithEvents Label_GenotypeName As System.Windows.Forms.Label
    Friend WithEvents Label_Rep As System.Windows.Forms.Label
    Friend WithEvents ContextMenu1 As System.Windows.Forms.ContextMenu
    Friend WithEvents MenuItemCopy As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItemPaste As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItemClose As System.Windows.Forms.MenuItem
    Friend WithEvents PanelInfoExtra As System.Windows.Forms.Panel
    Friend WithEvents PanelCollectingOptions As System.Windows.Forms.Panel
    Friend WithEvents Label_CollectingOptions As System.Windows.Forms.Label
    Friend WithEvents Label_InputForm As System.Windows.Forms.Label
    Friend WithEvents CheckBox_InputForm As System.Windows.Forms.CheckBox
    Friend WithEvents Panel_InputForm As System.Windows.Forms.Panel
    Friend WithEvents Button_InputFormOK As System.Windows.Forms.Button
    Friend WithEvents Button_InputFormCancel As System.Windows.Forms.Button
    Friend WithEvents Label_LabelDesign As System.Windows.Forms.Label
    Friend WithEvents NumericUpDownCantidad As System.Windows.Forms.NumericUpDown
    Friend WithEvents ComboBox_LabelType As System.Windows.Forms.ComboBox
    Friend WithEvents Label_NumberOfLabels As System.Windows.Forms.Label
    Friend WithEvents Label_FrozenGridColsNumber As System.Windows.Forms.Label
    Friend WithEvents NumericUpDownFrozenColsNumber As System.Windows.Forms.NumericUpDown
    Friend WithEvents C1FlexGrid_InputForm As C1.Win.C1FlexGrid.C1FlexGrid
    Friend WithEvents PanelOthersOptions As System.Windows.Forms.Panel
    Friend WithEvents Label_OthersOptions As System.Windows.Forms.Label
    Friend WithEvents Label_Languages As System.Windows.Forms.Label
    Friend WithEvents ComboBox_Language As System.Windows.Forms.ComboBox
    Friend WithEvents Label_About As System.Windows.Forms.Label
    Friend WithEvents NumericUpDownFrozenRowsNumberFom As System.Windows.Forms.NumericUpDown
    Friend WithEvents Label_FrozenFormRowsNumber As System.Windows.Forms.Label
    Friend WithEvents Label_BlankValue As System.Windows.Forms.Label
    Friend WithEvents TextBox_BlankValue As System.Windows.Forms.TextBox
    Friend WithEvents CheckBox_ReplaceBlankValue As System.Windows.Forms.CheckBox
    Friend WithEvents Label_ReplaceBlankValue As System.Windows.Forms.Label
    Friend WithEvents CheckBox_Autosave As System.Windows.Forms.CheckBox
    Friend WithEvents Label_Autosave As System.Windows.Forms.Label
    Friend WithEvents CheckBox_SearchForSavedFiles As System.Windows.Forms.CheckBox
    Friend WithEvents Label_SearchForSavedFiles As System.Windows.Forms.Label
    Friend WithEvents Panel_SavedFiles As System.Windows.Forms.Panel
    Friend WithEvents Button_SFFormLoad As System.Windows.Forms.Button
    Friend WithEvents Button_SFFormCancel As System.Windows.Forms.Button
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents ListView_SavedFiles As System.Windows.Forms.ListView
    Friend WithEvents chkNotif As System.Windows.Forms.CheckBox
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents chkMissing As System.Windows.Forms.CheckBox
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents txtMissing As System.Windows.Forms.TextBox
    Friend WithEvents Label5 As System.Windows.Forms.Label
End Class
