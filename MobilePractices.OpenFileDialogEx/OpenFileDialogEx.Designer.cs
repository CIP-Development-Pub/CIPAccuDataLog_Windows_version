﻿//-----------------------------------------------------------------------
// 
//  Copyright (C) MOBILE PRACTICES.  All rights reserved.
//  http://www.mobilepractices.com/
//
// THIS CODE AND INFORMATION ARE PROVIDED AS IS WITHOUT WARRANTY OF ANY
// KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A
// PARTICULAR PURPOSE.
//-----------------------------------------------------------------------

namespace MobilePractices.OpenFileDialogEx
{
    partial class OpenFileDialogEx
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;
        private System.Windows.Forms.MainMenu mainMenu1;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(OpenFileDialogEx));
            this.mainMenu1 = new System.Windows.Forms.MainMenu();
            this.menuItem1 = new System.Windows.Forms.MenuItem();
            this.menuItem2 = new System.Windows.Forms.MenuItem();
            this.fileListView = new System.Windows.Forms.ListView();
            this.columnHeader1 = new System.Windows.Forms.ColumnHeader();
            this.FileSystemIcons = new System.Windows.Forms.ImageList();
            this.PathSelectorComboBox = new System.Windows.Forms.ComboBox();
            this.FileSystemIconsx32 = new System.Windows.Forms.ImageList();
            this.SuspendLayout();
            // 
            // mainMenu1
            // 
            this.mainMenu1.MenuItems.Add(this.menuItem1);
            this.mainMenu1.MenuItems.Add(this.menuItem2);
            // 
            // menuItem1
            // 
            this.menuItem1.Text = "Up";
            this.menuItem1.Click += new System.EventHandler(this.menuItem1_Click);
            // 
            // menuItem2
            // 
            this.menuItem2.Text = "Cancel";
            this.menuItem2.Click += new System.EventHandler(this.menuItem2_Click);
            // 
            // fileListView
            // 
            this.fileListView.Activation = System.Windows.Forms.ItemActivation.OneClick;
            this.fileListView.Columns.Add(this.columnHeader1);
            this.fileListView.Dock = System.Windows.Forms.DockStyle.Fill;
            this.fileListView.Font = new System.Drawing.Font("Tahoma", 10F, System.Drawing.FontStyle.Regular);
            this.fileListView.Location = new System.Drawing.Point(0, 24);
            this.fileListView.Name = "fileListView";
            this.fileListView.Size = new System.Drawing.Size(240, 244);
            this.fileListView.SmallImageList = this.FileSystemIconsx32;
            this.fileListView.TabIndex = 2;
            this.fileListView.View = System.Windows.Forms.View.SmallIcon;
            this.fileListView.ItemActivate += new System.EventHandler(this.fileListView_ItemActivate);
            // 
            // columnHeader1
            // 
            this.columnHeader1.Text = "ColumnHeader";
            this.columnHeader1.Width = 60;
            // 
            // FileSystemIcons
            // 
            this.FileSystemIcons.ImageSize = new System.Drawing.Size(32, 32);
            this.FileSystemIcons.Images.Clear();
            this.FileSystemIcons.Images.Add(((System.Drawing.Image)(resources.GetObject("resource6"))));
            this.FileSystemIcons.Images.Add(((System.Drawing.Image)(resources.GetObject("resource7"))));
            this.FileSystemIcons.Images.Add(((System.Drawing.Image)(resources.GetObject("resource8"))));
            this.FileSystemIcons.Images.Add(((System.Drawing.Image)(resources.GetObject("resource9"))));
            // 
            // PathSelectorComboBox
            // 
            this.PathSelectorComboBox.BackColor = System.Drawing.SystemColors.Control;
            this.PathSelectorComboBox.Dock = System.Windows.Forms.DockStyle.Top;
            this.PathSelectorComboBox.Font = new System.Drawing.Font("Tahoma", 10F, System.Drawing.FontStyle.Regular);
            this.PathSelectorComboBox.Location = new System.Drawing.Point(0, 0);
            this.PathSelectorComboBox.Name = "PathSelectorComboBox";
            this.PathSelectorComboBox.Size = new System.Drawing.Size(240, 24);
            this.PathSelectorComboBox.TabIndex = 1;
            this.PathSelectorComboBox.SelectedIndexChanged += new System.EventHandler(this.PathSelectorComboBox_SelectedIndexChanged);
            // 
            // FileSystemIconsx32
            // 
            this.FileSystemIconsx32.ImageSize = new System.Drawing.Size(32, 32);
            this.FileSystemIconsx32.Images.Clear();
            this.FileSystemIconsx32.Images.Add(((System.Drawing.Image)(resources.GetObject("resource"))));
            this.FileSystemIconsx32.Images.Add(((System.Drawing.Image)(resources.GetObject("resource1"))));
            this.FileSystemIconsx32.Images.Add(((System.Drawing.Image)(resources.GetObject("resource2"))));
            this.FileSystemIconsx32.Images.Add(((System.Drawing.Image)(resources.GetObject("resource3"))));
            this.FileSystemIconsx32.Images.Add(((System.Drawing.Image)(resources.GetObject("resource4"))));
            this.FileSystemIconsx32.Images.Add(((System.Drawing.Image)(resources.GetObject("resource5"))));
            // 
            // OpenFileDialogEx
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(96F, 96F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi;
            this.AutoScroll = true;
            this.BackColor = System.Drawing.SystemColors.ControlDark;
            this.ClientSize = new System.Drawing.Size(240, 268);
            this.ControlBox = false;
            this.Controls.Add(this.fileListView);
            this.Controls.Add(this.PathSelectorComboBox);
            this.Menu = this.mainMenu1;
            this.MinimizeBox = false;
            this.Name = "OpenFileDialogEx";
            this.Text = "OpenFileDialogEx";
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.ListView fileListView;
		private System.Windows.Forms.ComboBox PathSelectorComboBox;
		private System.Windows.Forms.ImageList FileSystemIcons;
		private System.Windows.Forms.MenuItem menuItem1;
		private System.Windows.Forms.MenuItem menuItem2;
        private System.Windows.Forms.ColumnHeader columnHeader1;
        private System.Windows.Forms.ImageList FileSystemIconsx32;
    }
}