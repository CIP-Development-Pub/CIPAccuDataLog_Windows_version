[![N|CIPOTATO](https://cldup.com/VE0DOR6SuE.png)](http://cipotato.org/)


# CIPAccuDataLog


CIPAccudatalog software is a solution for helping in the activity of collecting data in field or greenhouses and printing barcodes using Bluetooth. Developed with Visual Studio 2008 in VB. Using VSFlexGrid from ComponentOne.

### Issues in Requirements Elicitation

  - Create validator to record the number of characters in a field descriptor
    The number of entered characters should be exactly as indicated in the descriptor type.

  - Restrict the number of decimal places.

  - In the Options tab should add a check to enable a confirmation message when changing each data cell

  - Allow columns are readonly.

  - Allow copy a cell to multiple cells, the functionality should be similar to excel.

  - Create a validator that allows the entry of only odd numbers, required for morphological descriptors with international standards for potato and sweet potato.

  - In the Options tab should add a check that allows the entry of missing values.










